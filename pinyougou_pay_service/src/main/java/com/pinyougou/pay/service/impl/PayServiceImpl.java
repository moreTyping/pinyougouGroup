package com.pinyougou.pay.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.wxpay.sdk.WXPayUtil;
import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.mapper.TbPayLogMapper;
import com.pinyougou.pay.service.PayService;
import com.pinyougou.pojo.TbOrder;
import com.pinyougou.pojo.TbPayLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import util.HttpClient;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
@Service
@Transactional
public class PayServiceImpl implements PayService {

    @Value("${appid}")
    private String appid;//公众号id

    @Value("${partner}")
    private String partner;//商户号id

    @Value("${partnerkey}")
    private String partnerkey;//商户秘钥

    @Value("${notifyurl}")
    private String notifyurl;//回调地址


    @Override
    public Map<String, Object> createNative(String out_trade_no, String total_fee) throws Exception {
        //1、组装必填请求参数
        Map<String,String> paramMap = new HashMap<>();
        paramMap.put("appid",appid);
        paramMap.put("mch_id",partner);
        paramMap.put("nonce_str", WXPayUtil.generateNonceStr());//生成随机字符串
        paramMap.put("body","品优购");
        paramMap.put("out_trade_no",out_trade_no);
        paramMap.put("total_fee",total_fee);
        paramMap.put("spbill_create_ip","127.0.0.1");
        paramMap.put("notify_url",notifyurl);
        paramMap.put("trade_type","NATIVE");
        paramMap.put("product_id","1");
        //2、基于httpclient发起请求
        String paramXml = WXPayUtil.generateSignedXml(paramMap, partnerkey);
        System.out.println(paramXml);
        HttpClient httpClient = new HttpClient("https://api.mch.weixin.qq.com/pay/unifiedorder");
        httpClient.setHttps(true);
        httpClient.setXmlParam(paramXml);
        httpClient.post();

        //3、处理响应结果
        String resultXml = httpClient.getContent();
        System.out.println(resultXml);
        Map<String, String> resultMap = WXPayUtil.xmlToMap(resultXml);

        String code_url = resultMap.get("code_url");//二维码支付链接

        Map<String, Object> map= new HashMap<>();
        map.put("code_url",code_url);
        map.put("out_trade_no",out_trade_no);
        map.put("total_fee",total_fee);
        return map;
    }

    @Override
    public Map<String, String> queryPayStatus(String out_trade_no) throws Exception {
        //1、组装必填请求参数
        Map<String,String> paramMap = new HashMap<>();
        paramMap.put("appid",appid);
        paramMap.put("mch_id",partner);
        paramMap.put("nonce_str", WXPayUtil.generateNonceStr());//生成随机字符串
        paramMap.put("out_trade_no",out_trade_no);

        //2、基于httpclient发起请求
        String paramXml = WXPayUtil.generateSignedXml(paramMap, partnerkey);
        System.out.println(paramXml);
        HttpClient httpClient = new HttpClient("https://api.mch.weixin.qq.com/pay/orderquery");
        httpClient.setHttps(true);
        httpClient.setXmlParam(paramXml);
        httpClient.post();
        //3、处理响应结果
        String resultXml = httpClient.getContent();
        System.out.println(resultXml);
        Map<String, String> resultMap = WXPayUtil.xmlToMap(resultXml);

        return resultMap;
    }

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private TbPayLogMapper payLogMapper;

    @Autowired
    private TbOrderMapper orderMapper;

    @Override
    public TbPayLog getPayLogFromRedis(String userId) {
        return (TbPayLog) redisTemplate.boundHashOps("payLog").get(userId);
    }

    @Override
    public void updatePayStatus(String out_trade_no, String transaction_id) {
        //支付成功后，需要更新支付日志和订单的支付状态
        TbPayLog payLog = payLogMapper.selectByPrimaryKey(out_trade_no);
        /*
        `pay_time` datetime DEFAULT NULL COMMENT '支付完成时间',
		`transaction_id` varchar(30) DEFAULT NULL COMMENT '交易号码',  //微信返回，支付成功后需要更新的数据
		`trade_state` varchar(1) DEFAULT NULL COMMENT '交易状态',
         */
        payLog.setPayTime(new Date());
        payLog.setTransactionId(transaction_id);
        payLog.setTradeState("2");//已支付
        payLogMapper.updateByPrimaryKey(payLog);
        //更新订单状态
        //获取支付关联的订单id列表
        String orderList = payLog.getOrderList();
        String[] orderIds = orderList.split(",");
        for (String orderId : orderIds) {
            TbOrder tbOrder = orderMapper.selectByPrimaryKey(Long.parseLong(orderId));
            tbOrder.setStatus("2");//已支付
            tbOrder.setPaymentTime(new Date());
            orderMapper.updateByPrimaryKey(tbOrder);

        }
        //支付成功后，清空redis中基于用户名关联的支付日志信息
        redisTemplate.boundHashOps("payLog").delete(payLog.getUserId());
    }
}
