package com.pinyougou.user.controller;

import entity.Member;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import weibo4j.Oauth;
import weibo4j.Users;
import weibo4j.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/login")
public class LoginController {

    /**
     * 基于安全框架获取登录人登录名信息
     * {loginName:"admin"}
     */
    @RequestMapping("/getLoginName")
    public Map<String,String> getLoginName(){
        //基于安全框架获取登录人登录名
        String loginName = SecurityContextHolder.getContext().getAuthentication().getName();

        Map<String,String> map = new HashMap<>();
        map.put("loginName",loginName);

        return map;

    }


    /**
     * 新浪登录页面
     * @param request
     * @param response
     */
    @RequestMapping("/sinaLogin")
    //以下注解含义：允许来自http://item.pinyougou.com的请求访问到该方法，并运行携带cookie
//    @CrossOrigin(origins = "http://localhost:8980",allowCredentials = "true")
    public void sinaLogin(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.sendRedirect(new Oauth().authorize("code", ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 新浪回调页面
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/sinaLoginRedirect")
    public Member sinaLoginRedirect(HttpServletRequest request, HttpServletResponse response) {
        String code = request.getParameter("code");
        System.out.println(code);
        try {
            Oauth oauth = new Oauth();
            String token = oauth.getAccessTokenByCode(code).toString();
            System.out.println(token);
            String str[] = token.split(",");
            String accessToken = str[0].split("=")[1];
            System.out.println(accessToken);
            String str1[] = str[3].split("]");
            String uid = str1[0].split("=")[1];
            System.out.println(uid);
            Users um = new Users(accessToken);
            User user = um.showUserById(uid);
//            Member member = memberService.queryMemberByToken(accessToken);
//            if (member == null) {
//            Member member = new Member();
//            member.setStatus(true);
//            member.setToken(accessToken);
//            member.setNickName(user.getScreenName());
//            member.setHeadImg(user.getavatarLarge());
//                memberService.saveMember(member);
//            }
//            request.getSession().setAttribute("MEMBER_SESSION", member);
//            System.out.println(member.toString());
           response.sendRedirect("http://127.0.0.1:8083/index.html");
        } catch (Exception e) {
            e.printStackTrace();
        }
            return null;
    }

}
