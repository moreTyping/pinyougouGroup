package com.pinyougou.seckill.service;

import com.pinyougou.pojo.TbSeckillGoods;

import java.util.List;

public interface SeckillService {
    /**
     * 查询秒杀商品列表
     */
    public List<TbSeckillGoods> selectSeckillGoodsListFromRedis();

    /**
     * 基于秒杀商品id查询秒杀商品信息
     */
    public TbSeckillGoods findOne(Long seckillGoodsId);

    /**
     * 秒杀下单
     * @param userId
     * @param seckillGoodsId
     */
    void submitSeckillOrder(String userId, Long seckillGoodsId);
}
