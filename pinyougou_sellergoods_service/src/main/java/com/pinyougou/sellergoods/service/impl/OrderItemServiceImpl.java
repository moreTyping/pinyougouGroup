package com.pinyougou.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.mapper.TbItemCatMapper;
import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.mapper.TbOrderItemMapper;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.pojo.TbItemCat;
import com.pinyougou.pojo.TbOrderItem;

import com.pinyougou.sellergoods.service.OrderItemService;
import entity.EcharsEntity;
import entity.Order_OrderItemEntity;
import entity.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @Author:GuTianyu
 * @Description:
 * @CreateDate: 2019/4/11 0011 9:36
 */
@Service
@Transactional
public class OrderItemServiceImpl implements OrderItemService {
    @Override
    public List<TbOrderItem> findAll() {
        return null;
    }

    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        return null;
    }

    @Override
    public void add(TbOrderItem orderItem) {

    }

    @Override
    public void update(TbOrderItem orderItem) {

    }

    @Override
    public TbOrderItem findOne(Long id) {
        return null;
    }

    @Override
    public void delete(Long[] ids) {

    }

    @Override
    public PageResult findPage(TbOrderItem orderItem, int pageNum, int pageSize) {
        return null;
    }

    @Autowired
    private TbOrderItemMapper orderItemMapper;

    @Autowired
    private TbItemCatMapper itemCatMapper;

    @Autowired
    private TbItemMapper itemMapper;

    @Override
    public List<EcharsEntity> findSaleMoneyInMonth() {
        List<TbOrderItem> orderItemsList= orderItemMapper.findSaleMoneyInMonth();
        List<TbItem> itemList =new ArrayList<TbItem>();
        for (TbOrderItem orderItem : orderItemsList) {
            Long itemId = orderItem.getItemId();
            TbItem item = itemMapper.selectByPrimaryKey(itemId);
            item.setPrice(orderItem.getTotalFee());//这是根据itemId分组查询的总金额
            itemList.add(item);
        }
        List<TbItemCat> itemCats = itemCatMapper.findAll3Category();

        List<EcharsEntity> echarsEntityList=new ArrayList<EcharsEntity>();

        for (TbItemCat itemCat : itemCats) {
            String itemName = itemCat.getName();
            String name="";
            double totalSale=0;
            EcharsEntity echarsEntity = new EcharsEntity();
           //外循环，根据3级分类循环
            for (TbItem item : itemList) {
                //内循环，便利itemList,相同分类就累加金额（也就是销售额，自己组装的,放在了price）,这数据查询肯定是有值的！重点是分类
                if(itemCat.getId().equals(item.getCategoryid())){
                   //手机分类，如果没有,什么也不做，如果有，做累加，记录当前手机分类下value总销售额
                    totalSale+=item.getPrice().doubleValue();
                }
            }

             echarsEntity.setName(itemName);
             echarsEntity.setTotalSale(totalSale);

             if(echarsEntity.getTotalSale()>0){
                 //如果总销售额大于0，添加到集合，用于展示柱状图
                 echarsEntityList.add(echarsEntity);
             }

            }

        return echarsEntityList;

    }

    @Override
    public List<EcharsEntity> findCountByYear(String sellerid) {
        List<Order_OrderItemEntity> order_orderItemList=orderItemMapper.findCountByYear(sellerid);
        System.out.println(order_orderItemList);
        List<EcharsEntity> echarsEntityList= new ArrayList<EcharsEntity>();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        for(int i=11;i>=0;i--){
            EcharsEntity echarsEntity = new EcharsEntity();
            if(month-i>0){
                if((month-i)<10){
                    echarsEntity.setName(year + "年0" + (month - i) + "月");
                    echarsEntity.setValue(0L);
                }else{
                    echarsEntity.setName(year + "年" + (month - i) + "月");
                    echarsEntity.setValue(0L);
                }
            }else{
                if((month+12-i)<10){
                    echarsEntity.setName(year-1 + "年0" + (month+12-i) + "月");
                    echarsEntity.setValue(0L);
                }else{
                    echarsEntity.setName(year-1 + "年" + (month+12-i) + "月");
                    echarsEntity.setValue(0L);
                }
            }
            echarsEntityList.add(echarsEntity);//2018年05月 2018年06月 2018年07月 2018年08月 ...
        }


        //循环遍历，组装EcharsEntity
        for (EcharsEntity echarsEntity : echarsEntityList) {
            //定义value，统计总销量num
            long num=0L;
            for (Order_OrderItemEntity order_orderItemEntity : order_orderItemList) {
                Date paytime = order_orderItemEntity.getPaytime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd HH:mm:ss");
                String format = sdf.format(paytime);

                if(format.indexOf(echarsEntity.getName())!=-1){
                    num+=order_orderItemEntity.getNum();
                    echarsEntity.setValue(num);
                }
            }
        }


        return echarsEntityList;
    }


}
