package com.pinyougou.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.TbBrandMapper;
import com.pinyougou.pojo.TbBrand;
import com.pinyougou.pojo.TbBrandExample;
import com.pinyougou.sellergoods.service.BrandService;
import entity.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service //dubbo注解，有两个作用：BrandServiceImpl创建对象交由spring管理 ,作用二：发布dubbo服务
@Transactional
public class BrandServiceImpl implements BrandService {
    @Autowired
    private TbBrandMapper brandMapper;

    @Override
    public List<TbBrand> findAll() {
        return brandMapper.selectByExample(null);
    }

    @Override
    public PageResult findPage(Integer pageNum, Integer pageSize) {
        //基于pageHelper实现分页查询
        //设置分页条件   计算分页起始值：(pageNum-1)*pageSize
        PageHelper.startPage(pageNum,pageSize);
        Page<TbBrand> page = (Page<TbBrand>) brandMapper.selectByExample(null);
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public void add(TbBrand brand) {
        brandMapper.insert(brand);
    }

    @Override
    public TbBrand findOne(Long id) {
        return brandMapper.selectByPrimaryKey(id);
    }

    @Override
    public void update(TbBrand brand) {
        brandMapper.updateByPrimaryKey(brand);
    }

    @Override
    public void delete(Long[] ids) {
        for (Long id : ids) {
            brandMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public PageResult search(TbBrand brand, Integer pageNum, Integer pageSize) {
        //分页条件设置
        PageHelper.startPage(pageNum,pageSize);

        //设置条件的对象
        TbBrandExample example = new TbBrandExample();
        //组装查询条件的对象
        TbBrandExample.Criteria criteria = example.createCriteria();
        //设置条件查询
        if(brand!=null){
            //获取品牌名称查询条件
            String brandName = brand.getName();

            if(brandName!=null && !"".equals(brandName)){
                //页面输入了品牌名称查询条件，组装条件查询操作  %%
                criteria.andNameLike("%"+brandName+"%");
            }

            //获取品牌首字母
            String firstChar = brand.getFirstChar();

            if(firstChar!=null && !"".equals(firstChar)){
                //页面输入了品牌首字母查询条件，组装条件查询操作
                criteria.andFirstCharEqualTo(firstChar);
            }

        }
        Page<TbBrand> page = (Page<TbBrand>) brandMapper.selectByExample(example);

        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public List<Map> selectBrandOptions() {
        return brandMapper.selectBrandOptions();
    }
}
