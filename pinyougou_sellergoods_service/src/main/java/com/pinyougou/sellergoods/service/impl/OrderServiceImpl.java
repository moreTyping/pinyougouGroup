package com.pinyougou.sellergoods.service.impl;

import com.pinyougou.mapper.TbGoodsMapper;
import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.mapper.TbOrderItemMapper;
import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.pojo.*;
import com.pinyougou.sellergoods.service.OrderService;
import groupEntity.Order;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrderServiceImpl implements OrderService {
    @Autowired
    private TbOrderMapper tbOrderMapper;
    @Autowired
    private TbOrderItemMapper tbOrderItemMapper;
    @Autowired
    private TbGoodsMapper goodsMapper;
    @Override
    public List<Order> searchByCondition(Map<String, Object> searchMap) {

        return null;
    }

    //查询所有数据
    @Override
    public List<Order> findAll(String seller_id) {
        //创建一个集合保存指定商户的订单数据
        List<Order> orders = new ArrayList<>();
        //设置条件的对象
        TbOrderExample example = new TbOrderExample();
        //组装查询条件的对象
        example.createCriteria().andSellerIdEqualTo(seller_id);
        List<TbOrder> tbOrders = tbOrderMapper.selectByExample(example);
        //遍历tbOrders查询对应的商品订单项
        for (TbOrder tbOrder : tbOrders) {
            //获取订单id
            Long orderId = tbOrder.getOrderId();
            TbOrderItemExample example1 = new TbOrderItemExample();
            example1.createCriteria().andOrderIdEqualTo(orderId);
            List<TbOrderItem> list = tbOrderItemMapper.selectByExample(example1);
            Order order = new Order();
            order.setTbOrder(tbOrder);
            order.setTbOrderItem(list);
            //存入集合
            orders.add(order);
        }
        return  orders;
    }

    @Override
    public List<Map<String, String>> findAllItem() {
        return  goodsMapper.findAllToMap();

    }
}
