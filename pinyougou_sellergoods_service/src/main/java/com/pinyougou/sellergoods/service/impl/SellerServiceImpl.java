package com.pinyougou.sellergoods.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.TbSellerMapper;
import com.pinyougou.pojo.TbSeller;
import com.pinyougou.pojo.TbSellerExample;
import com.pinyougou.pojo.TbSellerExample.Criteria;
import com.pinyougou.sellergoods.service.SellerService;
import entity.PageResult;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 服务实现层
 * @author Administrator
 *
 */
@Service
public class SellerServiceImpl implements SellerService {

	@Autowired
	private TbSellerMapper sellerMapper;
	
	/**
	 * 查询全部
	 */
	@Override
	public List<TbSeller> findAll() {
		return sellerMapper.selectByExample(null);
	}

	/**
	 * 按分页查询
	 */
	@Override
	public PageResult findPage(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);		
		Page<TbSeller> page=   (Page<TbSeller>) sellerMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}

	/**
	 * 增加
	 */
	@Override
	public void add(TbSeller seller) {
		//设置商家状态  新入驻的商家都是未审核状态
		seller.setStatus("0");
		//设置创建时间，取服务器时间
		seller.setCreateTime(new Date());
		sellerMapper.insert(seller);
	}

	
	/**
	 * 修改
	 */
	@Override
	public void update(TbSeller seller){
		sellerMapper.updateByPrimaryKey(seller);
	}	
	
	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbSeller findOne(String id){
		return sellerMapper.selectByPrimaryKey(id);
	}

	/**
	 * 批量删除
	 */
	@Override
	public void delete(String[] ids) {
		for(String id:ids){
			sellerMapper.deleteByPrimaryKey(id);
		}		
	}
	
	
		@Override
	public PageResult findPage(TbSeller seller, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		
		TbSellerExample example=new TbSellerExample();
		Criteria criteria = example.createCriteria();
		
		if(seller!=null){			
			if(seller.getSellerId()!=null && seller.getSellerId().length()>0){
				criteria.andSellerIdLike("%"+seller.getSellerId()+"%");
			}
			if(seller.getName()!=null && seller.getName().length()>0){
				criteria.andNameLike("%"+seller.getName()+"%");
			}
			if(seller.getNickName()!=null && seller.getNickName().length()>0){
				criteria.andNickNameLike("%"+seller.getNickName()+"%");
			}
			if(seller.getPassword()!=null && seller.getPassword().length()>0){
				criteria.andPasswordLike("%"+seller.getPassword()+"%");
			}
			if(seller.getEmail()!=null && seller.getEmail().length()>0){
				criteria.andEmailLike("%"+seller.getEmail()+"%");
			}
			if(seller.getMobile()!=null && seller.getMobile().length()>0){
				criteria.andMobileLike("%"+seller.getMobile()+"%");
			}
			if(seller.getTelephone()!=null && seller.getTelephone().length()>0){
				criteria.andTelephoneLike("%"+seller.getTelephone()+"%");
			}
			if(seller.getStatus()!=null && seller.getStatus().length()>0){
				//criteria.andStatusLike("%"+seller.getStatus()+"%");
				criteria.andStatusEqualTo(seller.getStatus());
			}
			if(seller.getAddressDetail()!=null && seller.getAddressDetail().length()>0){
				criteria.andAddressDetailLike("%"+seller.getAddressDetail()+"%");
			}
			if(seller.getLinkmanName()!=null && seller.getLinkmanName().length()>0){
				criteria.andLinkmanNameLike("%"+seller.getLinkmanName()+"%");
			}
			if(seller.getLinkmanQq()!=null && seller.getLinkmanQq().length()>0){
				criteria.andLinkmanQqLike("%"+seller.getLinkmanQq()+"%");
			}
			if(seller.getLinkmanMobile()!=null && seller.getLinkmanMobile().length()>0){
				criteria.andLinkmanMobileLike("%"+seller.getLinkmanMobile()+"%");
			}
			if(seller.getLinkmanEmail()!=null && seller.getLinkmanEmail().length()>0){
				criteria.andLinkmanEmailLike("%"+seller.getLinkmanEmail()+"%");
			}
			if(seller.getLicenseNumber()!=null && seller.getLicenseNumber().length()>0){
				criteria.andLicenseNumberLike("%"+seller.getLicenseNumber()+"%");
			}
			if(seller.getTaxNumber()!=null && seller.getTaxNumber().length()>0){
				criteria.andTaxNumberLike("%"+seller.getTaxNumber()+"%");
			}
			if(seller.getOrgNumber()!=null && seller.getOrgNumber().length()>0){
				criteria.andOrgNumberLike("%"+seller.getOrgNumber()+"%");
			}
			if(seller.getLogoPic()!=null && seller.getLogoPic().length()>0){
				criteria.andLogoPicLike("%"+seller.getLogoPic()+"%");
			}
			if(seller.getBrief()!=null && seller.getBrief().length()>0){
				criteria.andBriefLike("%"+seller.getBrief()+"%");
			}
			if(seller.getLegalPerson()!=null && seller.getLegalPerson().length()>0){
				criteria.andLegalPersonLike("%"+seller.getLegalPerson()+"%");
			}
			if(seller.getLegalPersonCardId()!=null && seller.getLegalPersonCardId().length()>0){
				criteria.andLegalPersonCardIdLike("%"+seller.getLegalPersonCardId()+"%");
			}
			if(seller.getBankUser()!=null && seller.getBankUser().length()>0){
				criteria.andBankUserLike("%"+seller.getBankUser()+"%");
			}
			if(seller.getBankName()!=null && seller.getBankName().length()>0){
				criteria.andBankNameLike("%"+seller.getBankName()+"%");
			}
	
		}
		
		Page<TbSeller> page= (Page<TbSeller>)sellerMapper.selectByExample(example);		
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public void updateStatus(String sellerId, String status) {
		//   update tb_seller set status=? where seller_id=?
		TbSeller tbSeller = sellerMapper.selectByPrimaryKey(sellerId);
		tbSeller.setStatus(status);
		sellerMapper.updateByPrimaryKey(tbSeller);
	}

	/**
	 * 准备需要导出到报表的数据
	 * @return
	 */
	@Override
	public List<Map<String, Object>> exportData() {
		return sellerMapper.selectExportData();
	}

	@Override
	public List<TbSeller> readExcel(String path) throws IOException {
		//Excel读取
		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(path));
		//读取excel内容生成文档对象
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		HSSFSheet hssfSheet = wb.getSheetAt(0); // 获取第一个Sheet页
		//判断用户是不是 按照模板填写的
		if(hssfSheet.getRow(0).getPhysicalNumberOfCells()>0&&hssfSheet.getRow(1).getPhysicalNumberOfCells()!=10){
			throw new RuntimeException("请下载模板进行填写");
		}
		List<TbSeller> sellers = new ArrayList<>();

		if (hssfSheet != null) {
			for (int rowNum = 2; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
				HSSFRow hssfRow = hssfSheet.getRow(rowNum);

				if (hssfRow != null) {

					TbSeller seller = new TbSeller();

			//1.先判断 表格中是否有未填写的内容
			for(int i=0;i<hssfRow.getPhysicalNumberOfCells();i++){
				//获取每一个单元格  判断值
				HSSFCell cell = hssfRow.getCell(i);
				cell.setCellType(Cell.CELL_TYPE_STRING);
				String stringCellValue = cell.getStringCellValue();
				if(" ".equals(stringCellValue)){
					throw  new RuntimeException("有未填写的单元格内容");
				}else if("".equals(stringCellValue)){
					throw  new RuntimeException("有未填写的单元格内容");
				}

			}

					seller.setSellerId(hssfRow.getCell(0).getStringCellValue());


					seller.setName(hssfRow.getCell(1).getStringCellValue());
					seller.setNickName(hssfRow.getCell(2).getStringCellValue());
					seller.setTelephone(hssfRow.getCell(3).getStringCellValue());

					//由于poi读取excel表格  如果是数字的话  默认读出来的double  类型的  我需要提前设置其类型
					hssfRow.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
					HSSFCell statusCell = hssfRow.getCell(4);
					statusCell.setCellType(Cell.CELL_TYPE_STRING);
					String status = statusCell.getStringCellValue();
					//判断状态数据是否按照要求填写
					if("1".equals(status)||"0".equals(status)){
						seller.setStatus(status);
					}else{
						throw new RuntimeException("状态数据填写错误");
					}
					//封装剩余数据
					seller.setAddressDetail(hssfRow.getCell(5).getStringCellValue());

					seller.setLinkmanName(hssfRow.getCell(6).getStringCellValue());

					seller.setLinkmanQq(hssfRow.getCell(7).getStringCellValue());

					seller.setLinkmanMobile(hssfRow.getCell(8).getStringCellValue());

					seller.setLinkmanEmail(hssfRow.getCell(9).getStringCellValue());

					sellers.add(seller);
				}else{
					throw new RuntimeException("没填写内容或者表格中间有空行!!");
				}
			}
		}
		return  sellers;
	}

	@Override
	public void batchAdd(List<TbSeller> sellers,String mypath) {
		for (TbSeller seller : sellers) {
			sellerMapper.add(seller);
		}
		//保存成功之后  删除在主机本地创建的文件
		File file = new File(mypath);
		file.delete();
	}

}
