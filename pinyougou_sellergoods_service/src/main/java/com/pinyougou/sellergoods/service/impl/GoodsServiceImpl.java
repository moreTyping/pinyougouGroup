package com.pinyougou.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import entity.PageResult;
import entity.Result;
import groupEntity.Goods;
import com.pinyougou.mapper.*;
import com.pinyougou.pojo.*;
import com.pinyougou.pojo.TbGoodsExample.Criteria;
import com.pinyougou.sellergoods.service.GoodsService;
import groupEntity.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 服务实现层
 * @author Administrator
 *
 */
@Service
@Transactional
public class GoodsServiceImpl implements GoodsService {

	@Autowired
	private TbGoodsMapper goodsMapper;

	@Autowired
	private TbGoodsDescMapper goodsDescMapper;

	@Autowired
	private TbSellerMapper sellerMapper;

	@Autowired
	private TbTypeTemplateMapper TypeTemplateMapper;

	@Autowired
	private TbBrandMapper brandMapper;

	@Autowired
	private TbItemMapper itemMapper;



	
	/**
	 * 查询全部
	 */
	@Override
	public List<TbGoods> findAll() {
		return goodsMapper.selectByExample(null);
	}


	/**
	 * 增加
	 */
	@Override
	public void add(Goods goods) {
		TbGoods tbGoods = goods.getGoods();
		TbGoodsDesc goodsDesc = goods.getGoodsDesc();
		//      设置spu是否上架
		tbGoods.setIsMarketable("0");
		//      设置spu状态 审核状态 由运营商来管理
		tbGoods.setAuditStatus("0");
		//      设置spu是否删除 由商家进行管理
		tbGoods.setIsDelete("0");
//		将goods进行保存
		goodsMapper.insert(goods.getGoods());
//		设置goodsDes主键
		goodsDesc.setGoodsId(goods.getGoods().getId());

//保存spu复杂属性
		goodsDescMapper.insert(goods.getGoodsDesc());

//		先判断是否启用规格
		if(goods.getGoods().getIsEnableSpec().equals("0")){
			//没启用规格时 价格和库存以及标题 通过goods获取
			TbItem item=new TbItem();
			String title=tbGoods.getGoodsName();
			item.setTitle(title);
			//后端进行组装数据
			getTbItem(tbGoods,goodsDesc,item);
//			 `spec` varchar(200) DEFAULT NULL,
			item.setSpec("{}");
//			 `price` decimal(20,2) NOT NULL COMMENT '商品价格，单位为：元',
			item.setPrice(tbGoods.getPrice());
//			 `num` int(10) NOT NULL COMMENT '库存数量',
			item.setNum(999);
//			 `status` varchar(1) NOT NULL COMMENT '商品状态，1-正常，2-下架，3-删除',
			item.setStatus("1");
//			 `is_default` varchar(1) DEFAULT NULL,
			item.setIsDefault("1");
			itemMapper.insert(item);
		}else{
			//启用规格时 正常保存
			for (TbItem item : goods.getItemList()) {
				getTbItem(tbGoods,goodsDesc,item);
				//设置标题 产品名称加空格加规格选项
				String spec = item.getSpec();
				Map<String,String> specMap = JSON.parseObject(spec,Map.class);
				String specStr="";
				//循环遍历将规格选项转化成字符串
				Set<String> strings = specMap.keySet();
				for (String key : strings) {
					specStr+=specMap.get(key)+" ";
				}
				specStr= goods.getGoods().getGoodsName() + " " + specStr;
				//去除标题字符串两端多余空格
				specStr=specStr.trim();
				item.setTitle(specStr);
				itemMapper.insert(item);
			}
		}
	}

	private void getTbItem(TbGoods tbGoods, TbGoodsDesc goodsDesc, TbItem item){

		//设置sku的属性

//			设置goodsId
		item.setGoodsId(tbGoods.getId());
			String nickName = sellerMapper.selectByPrimaryKey(tbGoods.getSellerId().toString()).getNickName();
		item.setSeller(nickName);
//			设置sellerId
		item.setSellerId(tbGoods.getSellerId());
//			设置创建时间
		item.setCreateTime(new Date());
//			设置修改时间
		item.setUpdateTime(new Date());
//			设置三级分类
		item.setCategoryid(tbGoods.getCategory3Id());

//			????????????????????????????????????????????????????????????????????????????
		item.setCategory(TypeTemplateMapper.selectByPrimaryKey(tbGoods.getTypeTemplateId()).getName());

//			设置商品图片默认使用商品详情中的第一张图片的数据
			//？？？？？？？？？？？？？？？？？？？？？？？？？？需要修改 判断有没有上传图片 再取url的key的值 而不用遍历map集合
			List<Map> images = JSON.parseArray(goodsDesc.getItemImages(),Map.class);
			item.setImage((String) images.get(0).get("url"));
//			设置品牌
			TbBrand tbBrand = brandMapper.selectByPrimaryKey(tbGoods.getBrandId());
		item.setBrand(tbBrand.getName());

	}

	
	/**
	 * 修改
	 */
	@Override
	//修改方法
	public void update(Goods goods) {
		goods.getGoods().setAuditStatus("0");// 设置未审核状态:如果是经过修改的商品，需要新审核
		goodsMapper.updateByPrimaryKey(goods.getGoods());// 保存商品表
		goodsDescMapper.updateByPrimaryKey(goods.getGoodsDesc());// 保存商品扩展表
		// 删除原有的 sku 列表数据
		TbItemExample example = new TbItemExample();
		com.pinyougou.pojo.TbItemExample.Criteria criteria = example.createCriteria();
		criteria.andGoodsIdEqualTo(goods.getGoods().getId());
		itemMapper.deleteByExample(example);
		// 添加新的 sku 列表数据
		saveItemList(goods);// 插入商品 SKU 列表数据
	}

	private void saveItemList(Goods goods){
		if("1".equals(goods.getGoods().getIsEnableSpec())){
			for(TbItem item:   goods.getItemList()){
				//构建标题  SPU名称+ 规格选项值
				String title=goods.getGoods().getGoodsName();//SPU名称
				Map<String,Object> map=  JSON.parseObject(item.getSpec());
				for(String key:map.keySet()) {
					title+=" "+map.get(key);
				}
				item.setTitle(title);

				setItemValues(item,goods);

				itemMapper.insert(item);
			}
		}else{//没有启用规格

			TbItem item=new TbItem();
			item.setTitle(goods.getGoods().getGoodsName());//标题
			item.setPrice(goods.getGoods().getPrice());//价格
			item.setNum(99999);//库存数量
			item.setStatus("1");//状态
			item.setIsDefault("1");//默认
			item.setSpec("{}");//规格

			setItemValues(item,goods);

			itemMapper.insert(item);
		}

	}
	@Autowired
	private TbItemCatMapper itemCatMapper;
	private void setItemValues(TbItem item, Goods goods){
		//商品分类
		item.setCategoryid(goods.getGoods().getCategory3Id());//三级分类ID
		item.setCreateTime(new Date());//创建日期
		item.setUpdateTime(new Date());//更新日期

		item.setGoodsId(goods.getGoods().getId());//商品ID
		item.setSellerId(goods.getGoods().getSellerId());//商家ID

		//分类名称
		TbItemCat itemCat = itemCatMapper.selectByPrimaryKey(goods.getGoods().getCategory3Id());
		item.setCategory(itemCat.getName());
		//品牌名称
		TbBrand brand = brandMapper.selectByPrimaryKey(goods.getGoods().getBrandId());
		item.setBrand(brand.getName());
		//商家名称(店铺名称)
		TbSeller seller = sellerMapper.selectByPrimaryKey(goods.getGoods().getSellerId());
		item.setSeller(seller.getNickName());

		//图片
		List<Map> imageList = JSON.parseArray( goods.getGoodsDesc().getItemImages(), Map.class) ;
		if(imageList.size()>0){
			item.setImage( (String)imageList.get(0).get("url"));
		}

	}

	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	@Override
	public Goods findOne(Long id){
		Goods goods=new Goods();
		TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
		goods.setGoods(tbGoods);
		TbGoodsDesc tbGoodsDesc = goodsDescMapper.selectByPrimaryKey(id);
		goods.setGoodsDesc(tbGoodsDesc);

		//在public Goods findOne(Long id) 方法中增加，加载sku信息的内容
		TbItemExample example = new TbItemExample();
		com.pinyougou.pojo.TbItemExample.Criteria criteria = example.createCriteria();
		criteria.andGoodsIdEqualTo(id);// 查询条件： 商品 ID
		List<TbItem> itemList = itemMapper.selectByExample(example);
		goods.setItemList(itemList);

		return goods;
	}

	/**
	 * 批量删除
	 */
	@Override
	public void delete(Long[] ids) {
		for(Long id:ids){
			goodsMapper.deleteByPrimaryKey(id);
		}		
	}
	
	
		@Override
	public PageResult findPage(TbGoods goods, int pageNum, int pageSize ) {
		PageHelper.startPage(pageNum, pageSize);
		
		TbGoodsExample example=new TbGoodsExample();
		Criteria criteria = example.createCriteria();
		
		if(goods!=null){			
						if(goods.getSellerId()!=null && goods.getSellerId().length()>0){
				criteria.andSellerIdEqualTo(goods.getSellerId());
			}
			if(goods.getGoodsName()!=null && goods.getGoodsName().length()>0){
				criteria.andGoodsNameLike("%"+goods.getGoodsName()+"%");
			}
			if(goods.getAuditStatus()!=null && goods.getAuditStatus().length()>0){
				criteria.andAuditStatusEqualTo(goods.getAuditStatus());
			}
			if(goods.getIsMarketable()!=null && goods.getIsMarketable().length()>0){
				criteria.andIsMarketableLike("%"+goods.getIsMarketable()+"%");
			}
			if(goods.getCaption()!=null && goods.getCaption().length()>0){
				criteria.andCaptionLike("%"+goods.getCaption()+"%");
			}
			if(goods.getSmallPic()!=null && goods.getSmallPic().length()>0){
				criteria.andSmallPicLike("%"+goods.getSmallPic()+"%");
			}
			if(goods.getIsEnableSpec()!=null && goods.getIsEnableSpec().length()>0){
				criteria.andIsEnableSpecLike("%"+goods.getIsEnableSpec()+"%");
			}
			if(goods.getIsDelete()!=null && goods.getIsDelete().length()>0){
				criteria.andIsDeleteLike("%"+goods.getIsDelete()+"%");
			}
	
		}
		
		Page<TbGoods> page= (Page<TbGoods>)goodsMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public Result changeStatus(Long[] ids, String stu) {
		try{
			for (Long id : ids) {
				goodsMapper.update(id, stu);
			}
			return new Result(true,"审核成功");
		}catch(Exception exception){
			exception.printStackTrace();
			return new Result(false,"审核失败");
		}
	}

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	private Destination queueUpGoodsDestination;

	@Autowired
	private Destination queueDownGoodsDestination;

	@Autowired
    private Destination queueUpPageDestination;

	@Autowired
    private Destination queueDownPageDestination;



	@Override
	public Result changeIsMarketable(Long[] ids, String stu) {
		for (final Long id : ids) {
			TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
			if(tbGoods.getAuditStatus().equals("1")){
				tbGoods.setIsMarketable(stu);
				goodsMapper.updateByPrimaryKey(tbGoods);
				//上架
				if("1".equals(stu)){
					//通过消息中间件 将上架商品的id发布到activeMq中 用于修改索引库
					jmsTemplate.send(queueUpGoodsDestination, new MessageCreator() {
						@Override
						public Message createMessage(Session session) throws JMSException {
							return session.createTextMessage(id.toString());
						}
					});
					//生成静态页
                    jmsTemplate.send(queueUpPageDestination, new MessageCreator() {
                        @Override
                        public Message createMessage(Session session) throws JMSException {
                            return session.createTextMessage(id.toString());
                        }
                    });
				}else{
					//下架
					jmsTemplate.send(queueDownGoodsDestination, new MessageCreator() {
						@Override
						public Message createMessage(Session session) throws JMSException {
							return session.createTextMessage(id.toString());
						}
					});
					//删除静态页 queueDownPageDestination
                    jmsTemplate.send(queueDownPageDestination, new MessageCreator() {
                        @Override
                        public Message createMessage(Session session) throws JMSException {
                            return session.createTextMessage(id.toString());
                        }
                    });
				}
			}else{
				return new Result(false,"未通过审核的商品不能上架");
			}
		}


		return new Result(true,"上架成功");
	}
}
