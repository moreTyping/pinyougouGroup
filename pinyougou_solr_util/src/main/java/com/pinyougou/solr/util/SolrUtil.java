package com.pinyougou.solr.util;

import com.alibaba.fastjson.JSON;
import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.pojo.TbItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class SolrUtil {

    @Autowired
    private TbItemMapper itemMapper;

    @Autowired
    private SolrTemplate solrTemplate;

    /**
     * 将满足条件的商品数据从数据库导入索引库
     *  1、上架商品导入索引库  tb_goods  is_marketable='1'
        2、商品状态为1，正常状态 tb_item   status='1'
     */
    public void dataImport(){
        List<TbItem> itemList = itemMapper.findAllGrounding();

        for (TbItem item : itemList) {
            //规格动态域赋值
            //获取当前商品规格数据 {"网络":"电信3G","机身内存":"64G"}
            String spec = item.getSpec();
            Map<String,String> specMap = JSON.parseObject(spec, Map.class);
            item.setSpecMap(specMap);
        }

        //导入索引库
        solrTemplate.saveBeans(itemList);
        solrTemplate.commit();

    }
}
