package cn.itcast.test;

import com.pinyougou.pojo.TbItem;
import com.pinyougou.solr.util.SolrUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.Query;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.data.solr.core.query.SolrDataQuery;
import org.springframework.data.solr.core.query.result.ScoredPage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:spring/applicationContext*.xml")
public class SolrTest {

    @Autowired
    private SolrTemplate solrTemplate;

    @Autowired
    private SolrUtil solrUtil;

    /**
     * 数据导入
     */
    @Test
    public void dataImport(){
        solrUtil.dataImport();
    }

    /**
     * 保存数据（新增或者修改数据）
     */
    @Test
    public void saveTest(){
        TbItem item = new TbItem();
        item.setId(2L);
        item.setBrand("华为");
        item.setTitle("华为mate30 电信3G 64G  很不错");
        item.setSeller("华为旗舰店");
        solrTemplate.saveBean(item);
        solrTemplate.commit();//提交保存数据

    }

    /**
     * 基于id查询
     */
    @Test
    public void getById(){
        TbItem item = solrTemplate.getById(1L, TbItem.class);
        System.out.println(item.getId()+"  "+item.getBrand()+"  "+item.getTitle()+"  "+item.getSeller());
    }

    /**
     * 基于id删除
     */
    @Test
    public void deleteById(){
        solrTemplate.deleteById("1");
        solrTemplate.commit();
    }

    /**
     * 删除所有
     */
    @Test
    public void deleteAll(){
        SolrDataQuery query=new SimpleQuery("*:*");
        solrTemplate.delete(query);
        solrTemplate.commit();
    }

    /**
     * 批量保存数据
     */
    @Test
    public void saveBatchTest(){
        List<TbItem> itemList = new ArrayList<>();
        for(long i=1;i<=100;i++){
            TbItem item = new TbItem();
            item.setId(i);
            item.setBrand("华为");
            item.setTitle(i+"华为mate30 电信3G 64G  很不错");
            item.setSeller("华为"+i+"旗舰店");
            itemList.add(item);
        }
        solrTemplate.saveBeans(itemList);
        solrTemplate.commit();//提交保存数据

    }

    /**
     * 分页查询
     */
    @Test
    public void queryPage(){
        Query query = new SimpleQuery("*:*");

        //设置分页查询条件
        query.setOffset(2);//分页起始值 默认从0开始
        query.setRows(5);//每页查询记录数 默认查询10条数据

        //参数一：查询对象
        ScoredPage<TbItem> page = solrTemplate.queryForPage(query, TbItem.class);

        System.out.println("总页数："+page.getTotalPages());
        System.out.println("总记录数："+page.getTotalElements());

        //获取当前页列表数据
        List<TbItem> content = page.getContent();

        for (TbItem item : content) {
            System.out.println(item.getId()+"  "+item.getBrand()+"  "+item.getTitle()+"  "+item.getSeller());
        }
    }

    /**
     * 条件查询
     *  title中包含8，商家中包含6的数据
     */
    @Test
    public void multiQuery(){

        Query query = new SimpleQuery();

        //设置分页查询条件
        query.setOffset(0);//分页起始值 默认从0开始
        query.setRows(5);//每页查询记录数 默认查询10条数据

        //构建查询条件  title中包含8，商家中包含6的数据
        Criteria criteria = new Criteria("item_title").contains("8").and("item_seller").contains("6");

        //将构建的查询条件赋予查询对象
        query.addCriteria(criteria);

        //参数一：查询对象
        ScoredPage<TbItem> page = solrTemplate.queryForPage(query, TbItem.class);

        System.out.println("总页数："+page.getTotalPages());
        System.out.println("总记录数："+page.getTotalElements());

        //获取当前页列表数据
        List<TbItem> content = page.getContent();

        for (TbItem item : content) {
            System.out.println(item.getId()+"  "+item.getBrand()+"  "+item.getTitle()+"  "+item.getSeller());
        }

    }





}
