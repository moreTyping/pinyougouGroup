package util;


import org.apache.poi.hssf.usermodel.*;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

//表格工具类  用于生成excel表格
public class ExcelUtil {
    private static final Logger LOG    = LoggerFactory.getLogger(ExcelUtil.class);
    /**创建一个excel文件*/
    private static Workbook createWorkBoot(String title,
                                           String[] excelHeader, List<Map<String, Object>> list, String[] keys) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        //设置sheet的名字
        Sheet sheet = workbook.createSheet(list.get(0).get("sheetName").toString());
        /*设置表格宽度*/
        for(int i = 0; i < keys.length; i++){
            sheet.setColumnWidth(i, 35*200);
        }

        /*font样式设置字体大小,是否加粗*/
        Font titleFont = createFont(workbook, (short)20);
        Font headerFont = createFont(workbook, (short)12);
        Font bodyFont = createFont(workbook, (short)12);
        /*cell通用样式*/
        CellStyle titleStyle = createStyle(workbook, titleFont);
        CellStyle headerStyle = createStyle(workbook, headerFont);
        CellStyle bodyStyle = createStyle(workbook, bodyFont);

        // excel中当前行索引
        int index = 0;
        /*合并标题的单元格设置标题信息及样式 */
        //参数有  4个  参数：起始行号，终止行号， 起始列号，终止列号  都是类似数组的索引  从0开始到lenth-1结束
        sheet.addMergedRegion(new CellRangeAddress(index, index, index,
                excelHeader.length - 1));
        Row titleRow = sheet.createRow(index++);
        Cell titleCell = titleRow.createCell(0);
        titleCell.setCellValue(title);
        titleCell.setCellStyle(titleStyle);

        /*设置表格头信息及样式*/
        Row headerRow = sheet.createRow(index++);
        for(int i = 0; i < excelHeader.length; i++) {
            Cell headerCell = headerRow.createCell(i);
            headerCell.setCellValue(excelHeader[i]);
            headerCell.setCellStyle(headerStyle);
        }

        /*设置每行每 列的值及样式
         *Row为行,cell为方格
         *创建i*j个方格并设置对应的属性值*/
        for(int i = 1; i < list.size(); i++) {
            Row bodyRow = sheet.createRow(index++);
            for (int j = 0; j < keys.length; j++) {
                Cell bodyCell = bodyRow.createCell(j);
                if("status".equals(keys[j])){
                    Object temp = list.get(i).get(keys[j]);
                    if(temp==null){
                        temp="";
                    }else{
                        temp=temp.equals("1")?"审核通过":"未审核";
                        bodyCell.setCellValue(temp.toString());
                    }
                }else {
                    bodyCell.setCellValue(list.get(i).get(keys[j]) == null ?
                            " " : list.get(i).get(keys[j]).toString());
                }
                bodyCell.setCellStyle(bodyStyle);
            }
           FileOutputStream outputStream = new FileOutputStream("E:\\img\\10.xls");
            workbook.write(outputStream);
        }
        return workbook;
    }
    /**设置字体大小，颜色，样式，是否加粗*/
    private static Font createFont(Workbook workbook,
                                   short fontHeightInPoints) {
        Font font = workbook.createFont();
        //字体大小
        font.setFontHeightInPoints(fontHeightInPoints);
        //字体颜色
        font.setColor(IndexedColors.BLACK.getIndex());
        //字体样式
        font.setFontName("宋体");
        //是否加粗
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//设置字体加粗
        return font;
    }
    /**设置字体居中显示，背景色，边框*/
    private static CellStyle createStyle(Workbook workbook, Font font) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);
        //居中
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        //背景颜色
        cellStyle.setFillForegroundColor(IndexedColors.WHITE.index);
        cellStyle.setFillBackgroundColor(IndexedColors.WHITE.index);
        cellStyle.setFillPattern(HSSFCellStyle. NO_FILL);
        //边框
        cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyle.setBorderRight(CellStyle.BORDER_THIN);
        cellStyle.setBorderTop(CellStyle.BORDER_THIN);
        return cellStyle;
    }

    public static boolean exportExcel(HttpServletResponse response, List<Map<String, Object>> list) throws IOException {
        String fileName = list.get(0).get("fileName").toString();
        String[] excelHeader = (String [])list.get(0).get("excelHeader");
        String[] keys = (String [])list.get(0).get("keys");
        String title = list.get(0).get("title").toString();
        //字节数组输出流  将所有发送到输出流的数据在内存中创建字节缓冲区进行保存
        ByteArrayOutputStream tempData = new ByteArrayOutputStream();
        try {

            createWorkBoot(title, excelHeader, list, keys).write(tempData);
        } catch (IOException e) {
            LOG.error("将workbook中信息写入输出流时失败");
            return false;
        }
        //调用api toByteArray() 拷贝当前输出流的数据  保存到字节数组里面
        byte[] content = tempData.toByteArray();
        //获取一个输入流  读取字节数组中的内容  加载到内存缓冲区
        InputStream is = new ByteArrayInputStream(content);

        response.reset();
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");/*   response.setContentType("application/vnd.ms-excel;charset=utf-8");*/
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            /*response.setHeader("Content-Disposition", "attachment;filename="
                    + new String((fileName + ".xls").getBytes(), "iso-8859-1"));*/
            response.setHeader("Content-Disposition", "attachment;filename="
                    + new String((fileName + ".xls").getBytes(), "UTF-8"));
            ServletOutputStream sos = response.getOutputStream();
            bis = new BufferedInputStream(is);
            bos = new BufferedOutputStream(sos);
            byte[] buff = new byte[2048];
            int byteRead = 0;
            while (-1 != (byteRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, byteRead);
            }
        } catch (IOException e) {
            LOG.error("创建excel文件时失败");
            return false;
        } finally {
            if (bos != null)
                bos.close();
            if (bis != null)
                bis.close();
            if(is != null)
                is.close();
            if(tempData != null)
                tempData.close();
        }
        return true;
    }
    public static boolean createTemplate(HttpServletResponse response,String[] header) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        //设置sheet的名字
        Sheet sheet = workbook.createSheet("模板表");
        /*设置表格宽度*/
        for(int i = 0; i < header.length; i++){
            sheet.setColumnWidth(i, 35*200);
        }
        /*font样式设置字体大小,是否加粗*/
        Font titleFont = createFont(workbook, (short)20);
        Font headerFont = createFont(workbook, (short)12);
        Font bodyFont = createFont(workbook, (short)12);
        /*cell通用样式*/
        CellStyle titleStyle = createStyle(workbook, titleFont);
        CellStyle headerStyle = createStyle(workbook, headerFont);
        CellStyle bodyStyle = createStyle(workbook, bodyFont);
        // excel中当前行索引
        int index = 0;
        /*合并标题的单元格设置标题信息及样式 */
        //参数有  4个  参数：起始行号，终止行号， 起始列号，终止列号  都是类似数组的索引  从0开始到lenth-1结束
        sheet.addMergedRegion(new CellRangeAddress(index, index, index,
                header.length - 1));
        Row titleRow = sheet.createRow(0);
        Cell titleCell = titleRow.createCell(0);
        titleCell.setCellValue("用户模板表(填写内容时请删除提示!!)");
        titleCell.setCellStyle(titleStyle);
        /*设置表格头信息及样式*/
        Row headerRow = sheet.createRow(1);
        for(int i = 0; i < header.length; i++) {
            Cell headerCell = headerRow.createCell(i);
            headerCell.setCellValue(header[i]);
            headerCell.setCellStyle(headerStyle);
        }
        //设置正文第一行
        Row bodyRow = sheet.createRow(2);
        for(int i = 0; i < header.length; i++) {
            Cell bodyCell = bodyRow.createCell(i);
            bodyCell.setCellValue("必填");
            bodyCell.setCellStyle(bodyStyle);
        }
        //测试  测试本地查看模板数据
      /*  FileOutputStream outputStream = new FileOutputStream("E:\\img\\模板.xls");
        workbook.write(outputStream);*/
        //字节数组输出流  将所有发送到输出流的数据在内存中创建字节缓冲区进行保存
        ByteArrayOutputStream temp= new ByteArrayOutputStream();
        try {

            workbook.write(temp);
        } catch (IOException e) {
            LOG.error("将workbook中信息写入输出流时失败");
            return false;
        }
        //调用api toByteArray() 拷贝当前输出流的数据  保存到字节数组里面
        byte[] content = temp.toByteArray();
        //获取一个输入流  读取字节数组中的内容  加载到内存缓冲区
        InputStream is = new ByteArrayInputStream(content);

        response.reset();
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");/*   response.setContentType("application/vnd.ms-excel;charset=utf-8");*/
        BufferedInputStream bis = null;
        BufferedOutputStream bos =null;
        try {
            /*response.setHeader("Content-Disposition", "attachment;filename="
                    + new String((fileName + ".xls").getBytes(), "iso-8859-1"));*/
            response.setHeader("Content-Disposition", "attachment;filename="
                    + new String(( "商家表" + new Date().getTime() + ".xls").getBytes(), "UTF-8"));
            ServletOutputStream sos = response.getOutputStream();
            bis = new BufferedInputStream(is);
            bos = new BufferedOutputStream(sos);
            byte[] buff = new byte[2048];
            int byteRead = 0;
            while (-1 != (byteRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, byteRead);
            }
        } catch (IOException e) {
            LOG.error("创建excel文件时失败");
            return false;
        } finally {
            if (bos != null)
                bos.close();
            if (bis != null)
                bis.close();
            if(is != null)
                is.close();
            if(temp!= null)
                temp.close();
        }
        return true;
    }
}
