package com.pinyougou.seckill.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.pojo.TbSeckillGoods;
import com.pinyougou.seckill.service.SeckillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SeckillServiceImpl implements SeckillService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ThreadPoolTaskExecutor executor;

    @Autowired
    private CreateOrder createOrder;

    @Override
    public List<TbSeckillGoods> selectSeckillGoodsListFromRedis() {
        return  redisTemplate.boundHashOps("seckill_goods").values();
    }

    @Override
    public TbSeckillGoods findOne(Long seckillGoodsId) {
        return (TbSeckillGoods) redisTemplate.boundHashOps("seckill_goods").get(seckillGoodsId);
    }

    @Override
    public void submitSeckillOrder(String userId, Long seckillGoodsId) {
        //解决同一个人重复购买问题
        Boolean member = redisTemplate.boundSetOps("seckill_goods_" + seckillGoodsId).isMember(userId);
        if (member) {
            throw new RuntimeException("您已经抢购过该商品，不允许重复抢购");
        }

        //进一个线程，排队人数加1
        redisTemplate.boundValueOps("seckill_user_queue_"+seckillGoodsId).increment(1);

        //判断redis中针对当前商品是否还有库存  [1,1,1,1]  从队列中获取当前商品id标记
        Object obj = redisTemplate.boundListOps("seckill_goods_queue_" + seckillGoodsId).rightPop();
        if (obj==null) {
            throw new RuntimeException("很遗憾，秒杀商品售罄");
        }

        //获取秒杀商品 10   50
        TbSeckillGoods seckillGoods = (TbSeckillGoods) redisTemplate.boundHashOps("seckill_goods").get(seckillGoodsId);
        //如果秒杀商品没有了，或者库存为0
       /* if(seckillGoods==null || seckillGoods.getStockCount()==0){
            throw new RuntimeException("很遗憾，秒杀商品售罄");
        }*/
       //获取当前商品排队人数
        Long size = redisTemplate.boundValueOps("seckill_user_queue_" + seckillGoodsId).size();

        //当排队人数大于当前商品库存+20时，提醒后面排队人员没有机会抢到该商品
        Integer stockCount = seckillGoods.getStockCount();
        if (size>(stockCount+20)) {
            throw new RuntimeException("排队人数过多");
        }

        Map<String,Object> map = new HashMap<>();
        map.put("seckillGoodsId",seckillGoodsId);
        map.put("userId",userId);

        //将秒杀下单的任务存入redis队列
        redisTemplate.boundListOps("seckill_order_queue").leftPush(map);

        //开启多线程，执行保存订单操作
        executor.execute(createOrder);
    }
}
