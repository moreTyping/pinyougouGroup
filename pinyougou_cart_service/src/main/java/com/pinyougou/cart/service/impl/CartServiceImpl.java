package com.pinyougou.cart.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.cart.service.CartService;
import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.pojo.TbOrderItem;
import groupEntity.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Transactional
public class CartServiceImpl implements CartService {

    @Autowired
    private TbItemMapper itemMapper;

    /**
     * 先根据商品id查询该商品关联的商家，是否存在于购物车列表中
         1该商家对应购物车对象不存在与购物车列表
             创建购物车对象，再存入购物车列表中
             创建购物车对象时，需要指定该购物车商家信息，以及构建购物车商品列表和购物车商品对象，
             将购物车明细对象添加到购物车明细列表中，将购物车明细列表添加到购物车对象，将购物车对象
             添加到购物车列表中

         2该商家对应购物车对象存在与购物车列表
            判断该商品是否存在于购物车商品明细列表中
             1、如果该商品不存在于购物车明细列表
                创建购物车商品对象，再添加到购物车商品列表中

             2、如果该商品存在于购物车明细列表
                修改购物车明细对象的商品数量和小计金额
     */
    @Override
    public List<Cart> addItemToCartList(List<Cart> cartList, Long itemId, Integer num) {
        //先根据商品id查询该商品关联的商家
        TbItem item = itemMapper.selectByPrimaryKey(itemId);
        if(item==null){
            throw new RuntimeException("很遗憾，商品不存在");
        }

        if(!"1".equals(item.getStatus())){
            throw new RuntimeException("商品无效");
        }

        String sellerId = item.getSellerId();
        //判断该商家对应的购物车是否存在于购物车列表中，如果存在，返回该购物车对象
        Cart cart = searchCartBySellerId(cartList,sellerId);
        if (cart==null) { //该商家对应购物车对象不存在与购物车列表
            // 创建购物车对象
            cart = new Cart();
            //需要指定该购物车商家信息
            cart.setSellerId(sellerId);
            cart.setSellerName(item.getSeller());
            //构建购物车商品列表
            List<TbOrderItem> orderItemList = new ArrayList<>();
            //构建购物车商品对象
            TbOrderItem orderItem = createOrderItem(item,num);
            //将购物车明细对象添加到购物车明细列表中
            orderItemList.add(orderItem);
            //将购物车商品列表添加到购物车对象
            cart.setOrderItemList(orderItemList);
            //将购物车对象添加到购物车列表中
            cartList.add(cart);
        }else {//该商家对应购物车对象存在与购物车列表
            //判断该商品是否存在于购物车商品明细列表中,如果存在该商品，返回该商品
            List<TbOrderItem> orderItemList = cart.getOrderItemList();
            TbOrderItem orderItem = searchOrderItemByItemId(orderItemList,itemId);
            if(orderItem==null){
                //如果该商品不存在于购物车商品列表
                //构建购物车商品对象
                orderItem = createOrderItem(item,num);
                //添加到购物车商品列表中
                orderItemList.add(orderItem);
            }else {//如果该商品存在于购物车明细列表
                // 修改购物车明细对象的商品数量和小计金额
                orderItem.setNum(orderItem.getNum()+num);
                orderItem.setTotalFee(new BigDecimal(orderItem.getPrice().doubleValue()*orderItem.getNum()));

                //如果商品数量减至0，移除该商品
                if(orderItem.getNum()<=0){
                    orderItemList.remove(orderItem);
                }
                //如果该商家对应的购物车中商品列表中没有数据，移除该商品对应的购物车数据
                if(orderItemList.size()==0){
                    cartList.remove(cart);
                }
            }
        }

        return cartList;
    }

    /**
     * 判断该商品是否存在于购物车商品明细列表中,如果存在该商品，返回该商品
     * @param orderItemList
     * @param itemId
     * @return
     */
    private TbOrderItem searchOrderItemByItemId(List<TbOrderItem> orderItemList, Long itemId) {
        for (TbOrderItem orderItem : orderItemList) {
            if (orderItem.getItemId().longValue()==itemId.longValue()) {
                return orderItem;
            }
        }
        return null;
    }

    /**
     * 构建购物车商品对象
     * @param item
     * @param num
     * @return
     */
    private TbOrderItem createOrderItem(TbItem item, Integer num) {
        if(num<1){
            throw new RuntimeException("至少添加一件商品到购物车");
        }
        /**
         `item_id` bigint(20) NOT NULL COMMENT '商品id',
         `goods_id` bigint(20) DEFAULT NULL COMMENT 'SPU_ID',
         `title` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '商品标题',
         `price` decimal(20,2) DEFAULT NULL COMMENT '商品单价',
         `num` int(10) DEFAULT NULL COMMENT '商品购买数量',
         `total_fee` decimal(20,2) DEFAULT NULL COMMENT '商品总金额',
         `pic_path` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '商品图片地址',
         `seller_id` varchar(100) COLLATE utf8_bin DEFAULT NULL,
         */
        TbOrderItem orderItem = new TbOrderItem();
        orderItem.setItemId(item.getId());
        orderItem.setGoodsId(item.getGoodsId());
        orderItem.setTitle(item.getTitle());
        orderItem.setPrice(item.getPrice());
        orderItem.setNum(num);
        orderItem.setTotalFee(new BigDecimal(orderItem.getPrice().doubleValue()*orderItem.getNum()));
        orderItem.setPicPath(item.getImage());
        orderItem.setSellerId(item.getSellerId());
        return orderItem;

    }

    /**
     * 判断该商家对应的购物车是否存在于购物车列表中，如果存在，返回该购物车对象
     * @param cartList
     * @param sellerId
     * @return
     */
    private Cart searchCartBySellerId(List<Cart> cartList, String sellerId) {
        for (Cart cart : cartList) {
            if(sellerId.equals(cart.getSellerId())){
                return cart;
            }
        }
        return null;
    }

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void saveCartListToRedisBySessionId(String sessionId, List<Cart> cartList) {
        redisTemplate.boundValueOps(sessionId).set(cartList,7L, TimeUnit.DAYS);
    }

    @Override
    public List<Cart> selectCartListByKey(String key) {
        List<Cart> cartList = (List<Cart>) redisTemplate.boundValueOps(key).get();
        //如果redis中获取不到购物车列表
        if(cartList==null){
            cartList = new ArrayList<>();//[]
        }
        return cartList;
    }

    @Override
    public void saveCartListToRedisByUsername(String username, List<Cart> cartList) {
        redisTemplate.boundValueOps(username).set(cartList);
    }

    @Override
    public List<Cart> mergeCartList(List<Cart> cartList_sessionId, List<Cart> cartList_username) {
        //将登录前的购物车列表中的商品添加到登录后的购物车列表中
        for (Cart cart : cartList_sessionId) {
            List<TbOrderItem> orderItemList = cart.getOrderItemList();
            for (TbOrderItem orderItem : orderItemList) {
                Long itemId = orderItem.getItemId();
                Integer num = orderItem.getNum();
                //将登录前的购物车列表中的商品添加到登录后的购物车列表中
                cartList_username = addItemToCartList(cartList_username,itemId,num);
            }
        }
        return cartList_username;
    }

    @Override
    public void deleteCartList(String sessionId) {
        redisTemplate.delete(sessionId);
    }
}
