 //控制层 
app.controller('seckillController' ,function($scope,$controller ,$location ,$interval ,seckillService){
	
	$controller('baseController',{$scope:$scope});//继承

    //查询秒杀商品列表
    $scope.selectSeckillGoodsList=function () {
        seckillService.selectSeckillGoodsList().success(function (response) {
            //秒杀商品列表
            $scope.seckillGoodsList=response;
        })
    }

    //基于秒杀商品id查询秒杀商品信息
    $scope.findOne=function () {
        //获取秒杀商品首页传递的秒杀商品id值
        $scope.seckillGoodsId =  $location.search()["seckillGoodsId"];
        seckillService.findOne($scope.seckillGoodsId).success(function (response) {
            //秒杀商品对象
            $scope.seckillGoods=response;
            //完成距离结束时间倒计时效果演示
            //计算当前时间距离结束时间总秒数
           /* var endTime = new Date($scope.seckillGoods.endTime).getTime();////获取结束时间戳
            var nowTime= new Date().getTime();//获取当前时间戳
            var seconds = Math.floor((endTime-nowTime)/1000);

            var days = Math.floor(seconds/60/60/24);  // 剩余天数
            var hours =Math.floor((seconds - days*24*60*60)/60/60);//剩余小时
            var minutes = Math.floor((seconds - days*24*60*60 - hours*60*60)/60);//剩余分钟数据
            var second=seconds - days*24*60*60 - hours*60*60-minutes*60;//剩余秒数*/

            //计算出剩余时间
            var endTime = new Date($scope.seckillGoods.endTime).getTime();
            var nowTime = new Date().getTime();

            //剩余时间
            $scope.secondes =Math.floor( (endTime-nowTime)/1000 );

            var time =$interval(function () {
                if($scope.secondes>0){
                    //时间递减
                    $scope.secondes--;
                    //时间格式化
                    $scope.timeString=$scope.convertTimeString($scope.secondes);
                }else{
                    //结束时间递减
                    $interval.cancel(time);
                }
            },1000);

        })
    }

    //时间格式化方法
    $scope.convertTimeString=function (allseconds) {
        //计算天数
        var days = Math.floor(allseconds/(60*60*24));

        //小时
        var hours =Math.floor( (allseconds-(days*60*60*24))/(60*60) );

        //分钟
        var minutes = Math.floor( (allseconds-(days*60*60*24)-(hours*60*60))/60 );

        //秒
        var seconds = allseconds-(days*60*60*24)-(hours*60*60)-(minutes*60);

        //拼接时间
        var timString="";
        if(days>0){
            timString=days+"天:";
        }

        if(hours<10){
            hours="0"+hours;
        }
        if(minutes<10){
            minutes="0"+minutes;
        }
        if(seconds<10){
            seconds="0"+seconds;
        }
        return timString+=hours+":"+minutes+":"+seconds;
    }
    
    //秒杀下单
    $scope.submitSeckillOrder=function () {
        seckillService.submitSeckillOrder($scope.seckillGoodsId).success(function (response) {
            if(response.success){
                //秒杀下单抢购成功
                location.href="pay.html";
            }else {
                alert(response.message);
            }
        })
    }

    //demo演示 数值递减
   /* $scope.count=10;
    //参数一：定时器要处理的事情  参数二：每个多长时间执行定时器（单位：毫秒） 参数三：执行多少次
    $interval(function () {
        $scope.count--;
    }, 1000,10);*/


});	
