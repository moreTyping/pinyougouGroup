package com.pinyougou.seckill.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbSeckillGoods;
import com.pinyougou.seckill.service.SeckillService;
import entity.Result;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/seckill")
public class SeckillController {

    @Reference
    private SeckillService seckillService;

    /**
     * 查询秒杀商品列表
     */
    @RequestMapping("/selectSeckillGoodsList")
    public List<TbSeckillGoods> selectSeckillGoodsList(){
        return seckillService.selectSeckillGoodsListFromRedis();
    }

    /**
     * 基于秒杀商品id查询秒杀商品信息
     */
    @RequestMapping("/findOne")
    public TbSeckillGoods findOne(Long seckillGoodsId){
        return seckillService.findOne(seckillGoodsId);
    }

    /**
     * 秒杀下单
     */
    @RequestMapping("/submitSeckillOrder")
    public Result submitSeckillOrder(Long seckillGoodsId){
        try {
            //获取登录名
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();

            if ("anonymousUser".equals(userId)) {
                return new Result(false,"抢购前，请先登录");
            }

            seckillService.submitSeckillOrder(userId,seckillGoodsId);
            return new Result(true,"秒杀下单成功");
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false,e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"秒杀下单失败");
        }
    }
}
