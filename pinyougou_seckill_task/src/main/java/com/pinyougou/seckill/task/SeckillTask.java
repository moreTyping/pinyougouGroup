package com.pinyougou.seckill.task;

import com.pinyougou.mapper.TbSeckillGoodsMapper;
import com.pinyougou.pojo.TbSeckillGoods;
import com.pinyougou.pojo.TbSeckillGoodsExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class SeckillTask {

    @Autowired
    private TbSeckillGoodsMapper seckillGoodsMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 将满足条件的秒杀商品从数据库中同步的redis
     * 条件：
     *       审核通过
             有库存
             当前时间大于开始时间,并小于秒杀结束时间  即：正在秒杀的商品
     */
   // @Scheduled(cron = "0 55 15 * * ?")//每天15点55分钟执行
    @Scheduled(cron = "0/10 * * * * ?")//从0秒开始每隔10秒执行
    public void synchronizeSeckillGoodsToRedis(){
        TbSeckillGoodsExample example = new TbSeckillGoodsExample();

        example.createCriteria()
                .andStatusEqualTo("1")// 审核通过
                .andStockCountGreaterThan(0)//有库存
                .andStartTimeLessThanOrEqualTo(new Date())//当前时间大于开始时间
                .andEndTimeGreaterThanOrEqualTo(new Date());//当前时间小于秒杀结束时间
        List<TbSeckillGoods> seckillGoodsList = seckillGoodsMapper.selectByExample(example);

        for (TbSeckillGoods seckillGoods : seckillGoodsList) {
            redisTemplate.boundHashOps("seckill_goods").put(seckillGoods.getId(),seckillGoods);

            //基于redis队列，解决秒杀商品超卖问题
            Integer stockCount = seckillGoods.getStockCount();
            for(int i=0;i<stockCount;i++){
                //基于redis队列记录秒杀商品还剩余多少个  1号商品5个  [1,1,1,1,1]
                redisTemplate.boundListOps("seckill_goods_queue_"+seckillGoods.getId()).leftPush(seckillGoods.getId());
            }

        }

        System.out.println("synchronizeSeckillGoodsToRedis finished .......");

    }

}
