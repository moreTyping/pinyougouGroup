package entity;

import java.io.Serializable;

/**
 * @Author:GuTianyu
 * @Description:
 * @CreateDate: 2019/4/11 0011 0:08
 */
public class EcharsEntity implements Serializable {
    private String name;
    private Long value;
    private double totalSale;
    private String YearAndMonth;

    public String getYearAndMonth() {
        return YearAndMonth;
    }

    public void setYearAndMonth(String yearAndMonth) {
        YearAndMonth = yearAndMonth;
    }

    public double getTotalSale() {
        return totalSale;
    }

    public void setTotalSale(double totalSale) {
        this.totalSale = totalSale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
