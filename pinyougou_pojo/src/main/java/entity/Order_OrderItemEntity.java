package entity;

import java.util.Date;

/**
 * @Author:GuTianyu
 * @Description:
 * @CreateDate: 2019/4/11 0011 15:34
 */
public class Order_OrderItemEntity {
    private Long itemId;
    private Integer num;
    private Date paytime;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Date getPaytime() {
        return paytime;
    }

    public void setPaytime(Date paytime) {
        this.paytime = paytime;
    }
}
