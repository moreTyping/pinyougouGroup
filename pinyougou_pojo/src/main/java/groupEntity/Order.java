package groupEntity;

import com.pinyougou.pojo.TbOrder;
import com.pinyougou.pojo.TbOrderItem;

import java.io.Serializable;
import java.util.List;

/**
 * 商家订单  以及对应订单商品的组合实体类
 * 一笔订单  对应多个商品的订单
 */
public class Order implements Serializable {
    private TbOrder tbOrder;
    private List<TbOrderItem> tbOrderItem;

    @Override
    public String toString() {
        return "Order{" +
                "tbOrder=" + tbOrder +
                ", tbOrderItem=" + tbOrderItem +
                '}';
    }

    public TbOrder getTbOrder() {
        return tbOrder;
    }

    public void setTbOrder(TbOrder tbOrder) {
        this.tbOrder = tbOrder;
    }

    public List<TbOrderItem> getTbOrderItem() {
        return tbOrderItem;
    }

    public void setTbOrderItem(List<TbOrderItem> tbOrderItem) {
        this.tbOrderItem = tbOrderItem;
    }
}
