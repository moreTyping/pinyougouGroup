package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbBrand;
import com.pinyougou.sellergoods.service.BrandService;
import entity.PageResult;
import entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/*
@Controller
@ResponseBody*/
@RestController //相当于@Controller+@ResponseBody
@RequestMapping("/brand")
public class BrandController {
    //去注册中心找brand服务
    @Reference
    private BrandService brandService;

    /**
     * 查询所有品牌列表
     */
    @RequestMapping("/findAll")
    public List<TbBrand> findAll(){
        return brandService.findAll();
    }

    /**
     * 分页查询品牌列表
     *
     */
    @RequestMapping("/findPage")
    public PageResult findPage(Integer pageNum,Integer pageSize){
        return brandService.findPage(pageNum,pageSize);
    }

    /**
     * 新增品牌
     *  返回结果：1 是否新增成功的标记  2 提示信息
     */
    @RequestMapping("/add")
    public Result add(@RequestBody TbBrand brand){
        try {
            brandService.add(brand);
            return new Result(true,"新增成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"新增失败");
        }
    }

    /**
     * 修改数据时，先基于id查询数据做回显操作
     */
    @RequestMapping("/findOne")
    public TbBrand findOne(Long id){
        return brandService.findOne(id);
    }

    /**
     * 修改品牌
     *  返回结果：1 是否新增成功的标记  2 提示信息
     */
    @RequestMapping("/update")
    public Result update(@RequestBody TbBrand brand){
        try {
            brandService.update(brand);
            return new Result(true,"修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"修改失败");
        }
    }

    /**
     * 批量删除品牌
     *  返回结果：1 是否新增成功的标记  2 提示信息
     */
    @RequestMapping("/delete")
    public Result delete(Long[] ids){
        try {
            brandService.delete(ids);
            return new Result(true,"删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"删除失败");
        }
    }

    /**
     * 条件分页查询
     *  {total:10,rows:[]}
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody TbBrand brand, Integer pageNum, Integer pageSize){
        return brandService.search(brand,pageNum,pageSize);
    }

    /**
     * 查询模板关联的品牌列表数据
     *  注意：在项目开发中，如果可以使用对象封装的数据，我们也可以采用map封装
     *  []  ng-repeat
     */
    @RequestMapping("/selectBrandOptions")
    public List<Map> selectBrandOptions(){
        return brandService.selectBrandOptions();
    }

}
