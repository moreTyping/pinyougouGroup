package com.pinyougou.manager.controller;
import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbSeller;
import com.pinyougou.sellergoods.service.SellerService;
import entity.PageResult;
import entity.Result;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import util.ExcelUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * controller
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/seller")
public class SellerController {

	@Reference
	private SellerService sellerService;
	
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<TbSeller> findAll(){			
		return sellerService.findAll();
	}
	
	
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult  findPage(int page,int rows){			
		return sellerService.findPage(page, rows);
	}
	
	/**
	 * 增加
	 * @param seller
	 * @return
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody TbSeller seller){
		try {
			sellerService.add(seller);
			return new Result(true, "增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "增加失败");
		}
	}
	
	/**
	 * 修改
	 * @param seller
	 * @return
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody TbSeller seller){
		try {
			sellerService.update(seller);
			return new Result(true, "修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败");
		}
	}	
	
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public TbSeller findOne(String id){
		return sellerService.findOne(id);		
	}
	
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	public Result delete(String [] ids){
		try {
			sellerService.delete(ids);
			return new Result(true, "删除成功"); 
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败");
		}
	}
	
		/**
	 * 查询+分页
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody TbSeller seller, int page, int rows  ){
		return sellerService.findPage(seller, page, rows);		
	}

	/**
	 * 商家审核操作
	 * sellerId 商家id
	 * status 页面提交的审核状态
	 */
	@RequestMapping("/updateStatus")
	public Result updateStatus(String sellerId,String status){
		try {
			sellerService.updateStatus(sellerId,status);
			return new Result(true, "审核成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "审核失败");
		}
	}
	//导出excel表
	@RequestMapping("/exportData")
	public void exportData(HttpServletResponse response){

		List<Map<String, Object>> sellerList = sellerService.exportData();
		String[] keys = {"sellerId", "name","nickName", "telephone", "status","addressDetail","linkmanName","linkmanQq","linkmanMoblie","linkmanEmail"};
		String[] excelHeader = {"用户ID","公司名","店铺名称","公司电话","状态","公司详细地址","联系人姓名","联系人QQ","联系人电话","联系人email"};
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("fileName", "商家表" + new Date().getTime());
		model.put("sheetName", "表一");
		model.put("title", "商家表");
		model.put("keys", keys);
		model.put("excelHeader", excelHeader);
		/**
		 *  select seller_id sellerId,name name,nick_name nickName,
		 telephone telephone,status status,address_detail addressDetail,linkman_name linkmanName,
		 linkman_qq linkmanQq,linkman_mobile linkmanMoblie,linkman_email linkmanEmail
		 from tb_seller
		 */
		list.add(model);
		for (Map<String, Object> map : sellerList) {
			list.add(map);
		}
		try {
			boolean b = ExcelUtil.exportExcel(response,list);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 导入(特定)excel表  Service 进行解析  更新数据到数据库
	 */
	@RequestMapping(value = "/importExcel",method = RequestMethod.POST)
	public Result importExcel(MultipartFile file, HttpServletRequest request)
			throws IOException {
		try {
			//获得上传文件的源文件名 例如 1.jps
			String originalFilename = file.getOriginalFilename();
			//保存在本地的目的是为了获取路径
			String mypath = "E:\\img\\" + originalFilename;

			//上传到本地
			file.transferTo(new File(mypath));


			//在service层进行解析
			List<TbSeller> sellers = sellerService.readExcel(mypath);
			//调用service  存入数据
			sellerService.batchAdd(sellers,mypath);
			return  new Result(true,"导入成功");
		} catch (IOException e) {
			e.printStackTrace();
			return  new Result(false,"导入失败");
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return  new Result(false,"导入失败");
		}catch (RuntimeException r){
			return  new Result(false,r.getMessage());
		}

	}
	/**
	 * 创建模板
	 */
	@RequestMapping("/createTemplate")
	public void createTemplate(HttpServletResponse response){
		String[] excelHeader = {"用户ID","公司名","店铺名称","公司电话","状态(0是未审核,1是已审核)","公司详细地址","联系人姓名","联系人QQ","联系人电话","联系人email"};
		try {
			ExcelUtil.createTemplate(response,excelHeader);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}


