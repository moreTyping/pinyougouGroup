package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbItemCat;

import com.pinyougou.sellergoods.service.ItemCatService;
import com.pinyougou.sellergoods.service.ItemService;
import com.pinyougou.sellergoods.service.OrderItemService;
import entity.EcharsEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * controller
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/home")
public class HomeController {

	@Reference
	private ItemService itemService;
	@Reference
	private ItemCatService itemCatService;

	/*
	 * 查询所有三级分类,根据三级分类查询item，统计name 和 value(num)
	 * */
	@RequestMapping("/findAll3Category")
	public List<EcharsEntity> findItemListAndNotNull(){

		List<TbItemCat> itemCats = itemCatService.findAll3Category();
		List<EcharsEntity> echarsEntityList= itemService.findItemListAndNotNull(itemCats);

		return echarsEntityList;
	}

	@Reference
	private OrderItemService orderItemService;

	/*
	* 查询近一个月3级分类下的销售额
	* */
	@RequestMapping("/findSaleMoneyInMonth")
	public List<EcharsEntity> findSaleMoneyInMonth(){
		List<EcharsEntity> saleMoneyInMonth = orderItemService.findSaleMoneyInMonth();
		return saleMoneyInMonth;
	}


}
