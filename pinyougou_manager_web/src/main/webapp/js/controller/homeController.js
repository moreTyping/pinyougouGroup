//定义控制器，为name模型数据赋值
//参数一：控制器名称 参数二：控制器要处理的事情
//$scope可以理解为全局的作用域对象  作用：相当于js代码与html代码数据交互的桥梁
//$http内置服务，作用：发起http请求 注意：发起的全部都是异步请求（ajax）
app.controller("homeController",function ($scope,$controller,loginService,homeService) {

    //完成控制器继承代码
    //参数一：父控制器名称 参数二：共享$scope固定写法
    $controller("baseController",{$scope:$scope});



    //获取登录名
    $scope.getLoginName=function () {
        loginService.getLoginName().success(function (response) {
            //response={loginName:"admin"}
            $scope.loginName=response.loginName;
        })
    }


    $scope.eChartsEntityList=[{name:{},value:0}];

    //获取所有三级分类，根据三级分类id查询sku(item)商品
    $scope.findAll3Category=function () {
        homeService.findAll3Category().success(function (response) {
            $scope.eChartsEntityList=response;
            console.log($scope.eChartsEntityList)
            showEchars();
        })

       showEchars=function () {
            //创建柱状图

            var myChart = echarts.init(document.getElementById('main1'));


            var selected=selectedFunction($scope.eChartsEntityList);
            function selectedFunction(eChartsEntityList) {
                var selected={};
                for(var i=0;i<eChartsEntityList.length;i++){
                    selected[eChartsEntityList[i].name] = i < eChartsEntityList.length;
                }
            }

            var data = {
                legendData:  $scope.eChartsEntityList.name,
                seriesData: $scope.eChartsEntityList,
                selected: selected
            };

            option = {
                title : {
                    text: '商品数量（饼状图）',
                    subtext: '总计',
                    x:'center'
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    type: 'scroll',
                    orient: 'vertical',
                    right: 10,
                    top: 20,
                    bottom: 20,
                    data: data.legendData,

                    selected: data.selected
                },
                series : [
                    {
                        name: '分类名称',
                        type: 'pie',
                        radius : '55%', //图饼半径
                        center: ['40%', '50%'],//图饼中心坐标
                        data: data.seriesData,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            //使用制定的配置项和数据显示图表
            myChart.setOption(option);


        }

    }
    
    $scope.findSaleMoneyInMonth=function () {
        homeService.findSaleMoneyInMonth().success(function (response) {
            $scope.eChartsEntityList2=response;
            showEchars2();
        })
        showEchars2=function () {

            var nameList=[];
            var saleList=[];
            //组装名称和销售额数组
            for(var i=0;i< $scope.eChartsEntityList2.length;i++){
                nameList.push( $scope.eChartsEntityList2[i].name)
                saleList.push( $scope.eChartsEntityList2[i].totalSale)
            }

            // 基于准备好的dom，初始化echarts实例
            var myChart2 = echarts.init(document.getElementById('main2'));

            // 指定图表的配置项和数据
            var option = {
                title: {
                    text: '月度统计销售额'
                },
                tooltip: {},
                legend: {
                    data:['销售额']
                },
                xAxis: {
               //   data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
                    data: nameList
                },
                yAxis: {},
                series: [{
                    name: '销售额',
                    type: 'bar',
                  //data: [5, 20, 36, 10, 10, 20]
                    data: saleList
                }]
            };

            // 使用刚指定的配置项和数据显示图表。
            myChart2.setOption(option);

        }

    }





});
