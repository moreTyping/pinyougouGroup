//定义控制器，为name模型数据赋值
//参数一：控制器名称 参数二：控制器要处理的事情
//$scope可以理解为全局的作用域对象  作用：相当于js代码与html代码数据交互的桥梁
//$http内置服务，作用：发起http请求 注意：发起的全部都是异步请求（ajax）
app.controller("specificationController",function ($scope,$controller,specificationService) {

    //完成控制器继承代码
    //参数一：父控制器名称 参数二：共享$scope固定写法
    $controller("baseController",{$scope:$scope});

    //查询所有品牌列表的方法
    $scope.findAll=function () {
        //参数：请求地址  success请求成功后回调函数  response接收响应结果
        specificationService.findAll().success(function (response) {
            $scope.list=response;
        })
    }

    //发起分页查询请求
    $scope.findPage=function (pageNum,pageSize) {
        specificationService.findPage(pageNum,pageSize).success(function (response) {
            $scope.list=response.rows;//当前页结果集
            $scope.paginationConf.totalItems=response.total;//总记录数
        })
    }

    //初始化条件查询对象
    $scope.searchEntity={};

    //条件分页查询请求
    $scope.search=function (pageNum,pageSize) {
        specificationService.search($scope.searchEntity,pageNum,pageSize).success(function (response) {
            $scope.list=response.rows;//当前页结果集
            $scope.paginationConf.totalItems=response.total;//总记录数
        })
    }
    //初始化规格组合实体对象
    $scope.entity={specification:{},specificationOptions:[]};
    //保存品牌
    $scope.save=function () {
        var method = null;
        //判断entity对象的id值是否存在，如果存在，说明是修改操作，如果不存在，说明是添加操作
        if($scope.entity.specification.id!=null){
            //如果存在，说明是修改操作
            method=specificationService.update($scope.entity);
        }else {
            //如果不存在，说明是添加操作
            method=specificationService.add($scope.entity);
        }

        //当请求参数时对象格式时，需要使用post请求
        //使用post请求时，参数二是需要提交的对象数据
        method.success(function (response) {
            //判断保存品牌是否成功
            if(response.success){
                //保存成功，重新加载列表数据
                $scope.reloadList();
            }else{
                //保存失败
                alert(response.message);
            }
        })
    }

    //基于id查询数据
    $scope.findOne=function (id) {
        //参数：请求地址  success请求成功后回调函数  response接收响应结果
        specificationService.findOne(id).success(function (response) {
            $scope.entity=response;
        })
    }

    //批量删除操作
    $scope.dele=function () {
        if(confirm("您确定要删除吗？")){
            specificationService.dele($scope.selectIds).success(function (response) {
                //判断删除品牌是否成功
                if(response.success){
                    //删除成功，重新加载列表数据
                    $scope.reloadList();
                    $scope.selectIds=[];//清除记录id的数组
                }else{
                    //保存失败
                    alert(response.message);
                }
            })
        }

    }

    //新增规格选项行
    $scope.addRow=function () {
        $scope.entity.specificationOptions.push({});
    }

    //删除规格选项行
    $scope.deleRow=function (index) {
        $scope.entity.specificationOptions.splice(index,1);
    }


});
