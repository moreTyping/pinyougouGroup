package com.pinyougou.sellergoods.service;

import entity.PageResult;
import groupEntity.Order;

import java.util.List;
import java.util.Map;

public interface OrderService {
    //条件查询所有的订单数据源
    public List<Order> searchByCondition(Map<String,Object> searchMap);
    //1.查询该商家所有订单信息
    public List<Order> findAll(String seller_id);
    //查询spu数据
    List<Map<String,String>> findAllItem();
}
