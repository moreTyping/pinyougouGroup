package com.pinyougou.pay.task;

import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.mapper.TbSeckillOrderMapper;
import com.pinyougou.pojo.TbOrder;
import com.pinyougou.pojo.TbOrderExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class PayTask {
    @Autowired
    private TbOrderMapper orderMapper;
    @Autowired
    private TbSeckillOrderMapper seckillOrderMapper;
    //每隔60秒执行一次
    @Scheduled(cron = "0/60 * * * * ?")
    public void timedClearanceOrder(){
        try {
            //修改超时订单
            orderMapper.updateTimeoutOrders();
            seckillOrderMapper.updateTimeoutOrders();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
