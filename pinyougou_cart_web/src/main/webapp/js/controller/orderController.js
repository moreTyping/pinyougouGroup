 //控制层 
app.controller('orderController' ,function($scope,$controller   ,cartService,addressService,orderService){
	
	$controller('baseController',{$scope:$scope});//继承

	//定义要寄送至的收件人地址对象
	$scope.address=null;

	//查询收件人地址列表
	$scope.findByUserId=function () {
		addressService.findByUserId().success(function (response) {
			//接收后端返回的地址列表
			$scope.addressList=response;
			//指定默认收件人地址
			for(var i=0;i<$scope.addressList.length;i++){
				if($scope.addressList[i].isDefault=='1'){
                    $scope.address=$scope.addressList[i];
                    break;
				}
			}

			//如果收件人没有指定默认地址，将地址列表中第一个作为默认地址
			if($scope.address==null){
                $scope.address=$scope.addressList[0];
			}
        })
    }

    //判断地址是否勾选的方法
	$scope.isSelect=function (addr) {
		if($scope.address==addr){
			return true;
		}else {
			return false;
		}
    }

    //收件人地址切换功能
	$scope.updateAddress=function (addr) {
		$scope.address=addr;
    }

    //初始化订单对象 默认是微信支付方式
	$scope.entity={paymentType:'1'};
    
    //切换支付方式
	$scope.updatePaymentType=function (type) {
        $scope.entity.paymentType=type;
    }

	
    //查询购物车列表
	$scope.findCartList=function(){
		cartService.findCartList().success(
			function(response){
				$scope.cartList=response;
                sum();
			}			
		);
	}  

    //统计商品总数量和总金额
	sum=function () {
		//定义商品总数量和总金额
		$scope.totalNum=0;
		$scope.totalMoney=0.00;
		//从购物车列表中获取每件商品的数量和小计，做累加操作
		for(var i=0;i<$scope.cartList.length;i++){
			var cart= $scope.cartList[i];
			var orderItemList =cart.orderItemList;//获取商品列表
			for(var j=0;j<orderItemList.length;j++){
                $scope.totalNum+=orderItemList[j].num;
                $scope.totalMoney+=orderItemList[j].totalFee;
			}
		}

    }

    //提交订单
	$scope.submitOrder=function () {
        /*
		 * `receiver_area_name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '收货人地区名称(省，市，县)街道',
         `receiver_mobile` varchar(12) COLLATE utf8_bin DEFAULT NULL COMMENT '收货人手机',
         `receiver` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '收货人',
         */
        $scope.entity.receiverAreaName=$scope.address.address;
        $scope.entity.receiverMobile=$scope.address.mobile;
        $scope.entity.receiver=$scope.address.contact;
		orderService.add($scope.entity).success(function (response) {
			if(response.success){
				//保存订单成功，跳转支付页面
				location.href="pay.html";
			}else{
				alert(response.message);
			}
        })
    }
	

});	
