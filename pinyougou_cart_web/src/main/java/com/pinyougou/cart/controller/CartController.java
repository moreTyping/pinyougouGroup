package com.pinyougou.cart.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.cart.service.CartService;
import entity.Result;
import groupEntity.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import util.CookieUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {

    @Reference
    private CartService cartService;

    @Autowired
    private HttpSession session;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

    private String getSessionId(){
        //1从系统中获取基于cookie保存的sessionId值
        String sessionId = CookieUtil.getCookieValue(request, "cartCookie");
        if(sessionId==null){
            //系统记录的sessionId过期
            sessionId=session.getId();
            //在重新存入系统的cookie数据中
            CookieUtil.setCookie(request,response,"cartCookie",sessionId,3600*24*7,"utf-8");
        }
        return sessionId;
    }

    /**
     * 查询购物车列表数据
     */
    @RequestMapping("/findCartList")
    public List<Cart> findCartList(){
        //获取登录人用户名username
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        //1、未登录，基于sessionId作为key从redis中获取购物车列表
        String sessionId = getSessionId();
        List<Cart> cartList_sessionId = cartService.selectCartListByKey(sessionId);
        if("anonymousUser".equals(username)){
            return cartList_sessionId;
        }else{
            //2、已登录，基于username作为key从redis中获取购物车列表
            List<Cart> cartList_username = cartService.selectCartListByKey(username);
            //当登录成功后，先判断登录前是否添加过商品到购物车列表中。
            //如果添加了，需要合并登录前的购物车列表到登录后的购物车列表中
            if(cartList_sessionId.size()>0){//登录前添加了商品到购物车列表
                //合并登录前的购物车列表到登录后的购物车列表中
                cartList_username = cartService.mergeCartList(cartList_sessionId,cartList_username);
                //清除登录前的购物车列表数据 基于sessionId清除
                cartService.deleteCartList(sessionId);
                //将合并后的购物车列表重新存入redis中
                cartService.saveCartListToRedisByUsername(username,cartList_username);
            }
            return cartList_username;
        }

    }

    /**
     * 添加商品到购物车列表
     */
    @RequestMapping("/addItemToCartList")
    //以下注解含义：运行来着http://item.pinyougou.com的请求访问到该方法，并运行携带cookie
    @CrossOrigin(origins = "http://item.pinyougou.com",allowCredentials = "true")
    public Result addItemToCartList(Long itemId,Integer num){
        try {
            //获取登录人用户名username
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            System.out.println(username);

            //获取sessionId
            String sessionId=getSessionId();

            //1、从redis中获取购物车列表
            List<Cart> cartList = findCartList();
            //2、添加商品到购物车列表
            cartList = cartService.addItemToCartList(cartList,itemId,num);

            if("anonymousUser".equals(username)){
                //未登录
                //3、将步骤2返回的购物车列表，重新存入redis ,基于sessionId作为key
                cartService.saveCartListToRedisBySessionId(sessionId,cartList);
            }else {//已经登录
                //3、将步骤2返回的购物车列表，重新存入redis ,基于username作为key
                cartService.saveCartListToRedisByUsername(username,cartList);
            }

            return new Result(true,"添加商品到购物车成功");
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false,e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"添加商品到购物车失败");
        }
    }
}
