package com.pinyougou.cart.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pay.service.PayService;
import com.pinyougou.pojo.TbPayLog;
import entity.Result;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/pay")
public class PayController {

    @Reference
    private PayService payService;

    /**
     * 生成二维码
     */
    @RequestMapping("/createNative")
    public Map<String,Object> createNative(){
        try {
            //基于用户名作为key，从redis中获取支付日志
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
           TbPayLog payLog = payService.getPayLogFromRedis(userId);
            return payService.createNative(payLog.getOutTradeNo(),payLog.getTotalFee()+"");
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap<>();//{}
        }
    }

    /**
     * 查询支付状态
     */
    @RequestMapping("/queryPayStatus")
    public Result queryPayStatus(String out_trade_no){
        try {
            int count=1;
            while(true){
                //每隔3秒执行一次
                Thread.sleep(3000);

                //二维码有效时间是5分钟，超过5分钟，跳出循环，提醒支付超时
                count++;
                if (count>100) {
                    return new Result(false,"timeout");
                }

                //获取支付状态响应结果
                Map<String, String> resultMap = payService.queryPayStatus(out_trade_no);
                String trade_state = resultMap.get("trade_state");
                if ("SUCCESS".equals(trade_state)) {
                    //支付成功后，需要更新支付日志和订单的支付状态
                    String transaction_id = resultMap.get("transaction_id");//获取微信交易流水号

                    payService.updatePayStatus(out_trade_no,transaction_id);

                    return new Result(true,"支付成功");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"支付失败");
        }
    }
}
