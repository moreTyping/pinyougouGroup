package com.pinyougou.page.listener;

import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.pojo.TbItemExample;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.io.File;
import java.util.List;

public class DeleItemPageMessageListener implements MessageListener{
    @Autowired
    private TbItemMapper itemMapper;


    @Override
    public void onMessage(Message message) {
        try {
            //商品下架，将同步删除下架商品静态页
            TextMessage textMessage = (TextMessage)message;
            //获取消息中上架商品id
            String goodsId = textMessage.getText();

            //查询下架商品列表
            TbItemExample example = new TbItemExample();
            TbItemExample.Criteria criteria = example.createCriteria();
            criteria.andGoodsIdEqualTo(Long.parseLong(goodsId));
            List<TbItem> itemList = itemMapper.selectByExample(example);

            for (TbItem item : itemList) {
                new File("f:\\item84\\"+item.getId()+".html").delete();
            }


        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
