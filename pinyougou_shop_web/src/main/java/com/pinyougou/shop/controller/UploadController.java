package com.pinyougou.shop.controller;

import entity.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import util.FastDFSClient;

@RestController
@RequestMapping("/upload")
public class UploadController {

    @Value("${FILE_SERVER_URL}")
    private String FILE_SERVER_URL;

    /**
     * 完成图片上传功能
     * MultipartFile springmvc接收上传文件的API
     */
    @RequestMapping("/uploadFile")
    public Result uploadFile(MultipartFile file){
        try {
            //获取源文件名 "1.jpg"
            String originalFilename = file.getOriginalFilename();
            String extName = originalFilename.substring(originalFilename.lastIndexOf(".")+1);

            //调用FastDFS工具类，实现文件上传操作
            FastDFSClient fastDFSClient = new FastDFSClient("classpath:config/fdfs_client.conf");
            //参数一：文件内容字节数组 参数二：文件扩展名
            // path=group1/M00/00/00/wKgZhVyQmsaABrGYAABeVgpCZ8s736.jpg
            String path = fastDFSClient.uploadFile(file.getBytes(), extName);

            //图片上传成功后，需要做图片回显，需要将图片加载地址返回
            String url =FILE_SERVER_URL+path;
            return new Result(true,url);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"图片上传失败");
        }
    }
}
