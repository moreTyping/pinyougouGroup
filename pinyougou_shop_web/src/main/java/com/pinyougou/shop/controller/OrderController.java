package com.pinyougou.shop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.sellergoods.service.OrderService;
import groupEntity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Reference
    private OrderService orderService;
    //查询所有数据
    @RequestMapping("/findAll")
    public List<Order> findAll(){
        //1.获取用户id
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        //2.调用服务层
        List<Order> orderList = orderService.findAll(userId);
        return orderList;
    }
    //查询sku商品数据
    @RequestMapping("/findAllItem")
    public List<Map<String,String>> findAllItem(){
        //1.调用服务层查询
       List<Map<String,String>> spuMap = orderService.findAllItem();
       return  spuMap;
    }
}
