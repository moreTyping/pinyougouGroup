 //控制层 
app.controller('goodsController' ,function($scope,$controller,$location,goodsService,itemCatService,typeTemplateService,fileUpLoadService){
	
	$controller('baseController',{$scope:$scope});//继承

    $scope.status=["未审核","审核通过","审核未通过","关闭"];
    $scope.upStatus=["下架","上架"];
    $scope.category=[""];

    //查找所有分类放到分类数组中用于显示
    $scope.getAllCategory=function () {
        itemCatService.getAllCategory().success(function (response) {
            for(var i=0;i<response.length;i++){
                $scope.category[response[i].id]=response[i].name;
            }
        })
    }

    //更改上下架的状态
    $scope.changeStatus=function (stu) {
        goodsService.changeStatus($scope.selectIds,stu).success(function (response) {
            if(response.success){
                $scope.reloadList();
                $scope.selectIds=[];
            }else{
                alert(response.message);
            }
        })
    }

    //读取列表数据绑定到表单中  
	$scope.findAll=function(){
        goodsService.findAll().success(
			function(response){
				$scope.list=response;
			}			
		);
	}    
	
	//分页
	$scope.findPage=function(page,rows){			
		goodsService.findPage(page,rows).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}

    // 查询实体
    $scope.findOne = function() {
        var id = $location.search()['id'];// 获取参数值
        if (id == null) {
            return;
        }

        goodsService.findOne(id).success(function(response) {
            $scope.entity = response;
            //向富文本编辑器添加商品介绍
            editor.html($scope.entity.goodsDesc.introduction);
            //显示图片列表
            $scope.entity.goodsDesc.itemImages=
                JSON.parse($scope.entity.goodsDesc.itemImages);
            //显示扩展属性
            $scope.entity.goodsDesc.customAttributeItems=
                JSON.parse($scope.entity.goodsDesc.customAttributeItems);
            //读取规格
            $scope.entity.goodsDesc.specificationItems=
                JSON.parse($scope.entity.goodsDesc.specificationItems);
            //准备SKU信息
            for( var i=0;i<$scope.entity.itemList.length;i++ ){
                $scope.entity.itemList[i].spec = JSON.parse( $scope.entity.itemList[i].spec);
            }
        });
    }
	
	//保存 
	$scope.save=function(){
		var serviceObject = null;//服务层对象
		if($scope.entity.goods.id!=null){//如果有ID
			serviceObject=goodsService.update( $scope.entity ); //修改
		}else{
			serviceObject=goodsService.add( $scope.entity);//增加商品
		}
        $scope.entity.goodsDesc.introduction=editor.html();
		serviceObject.success(
			function(response){
				if(response.success){
					//重新查询 
		        	//$scope.reloadList();//重新加载
                    //editor.html("");
                    location.href="goods.html";
				}else{
					alert(response.message);
				}
			}		
		);				
	}
	
	 
	//批量删除 
	$scope.dele=function(){			
		//获取选中的复选框			
		goodsService.dele( $scope.selectIds ).success(
			function(response){
				if(response.success){
					$scope.reloadList();//刷新列表
				}						
			}		
		);				
	}
	
	$scope.searchEntity={};//定义搜索对象 
	
	//搜索
	$scope.search=function(page,rows){			
		goodsService.search(page,rows,$scope.searchEntity).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}


    $scope.entity={goods:{isEnableSpec:1},goodsDesc:{},itemList:[]}

//	页面加载时查询一级分类
	$scope.selectCat1=function () {
		itemCatService.findAll(0).success(function (response) {
			$scope.itemCat1=response;
        })
    }

//    查询二级分类
//    添加监听事件 用于三级联动查询
	$scope.$watch("entity.goods.category1Id",function (newValue, oldValue) {
		itemCatService.findAll(newValue).success(function (response) {
			$scope.itemCat2=response;
		})
        $scope.itemCat3=null;
        $scope.entity.goods.typeTemplateId=null;
        $scope.brandList=null;
    })
//    查询三级分类
	$scope.$watch("entity.goods.category2Id",function (newValue, oldValue) {
		itemCatService.findAll(newValue).success(function (response) {
			$scope.itemCat3=response;
        })
        $scope.entity.goods.typeTemplateId=null;
        $scope.brandList=null;
    })

//	模板id查询 以及品牌列表 封装前台扩展属性 查找规格
    $scope.$watch("entity.goods.category3Id",function (newValue, oldValue) {

        //查找模板id
        itemCatService.findOne(newValue).success(function (response) {
            $scope.entity.goods.typeTemplateId=response.typeId;
            //根据模板id查找品牌列表以及扩展属性
            typeTemplateService.findOne(response.typeId).success(function (response2) {
            	$scope.brandList=JSON.parse(response2.brandIds);
            	if($location.search()['id'] ==null){
                    $scope.entity.goodsDesc.customAttributeItems=JSON.parse(response2.customAttributeItems);
                }
            });
            //   根据模板id查找规格
            typeTemplateService.findSpecifications(response.typeId).success(function (response) {
            	//
            	$scope.specifications=response;
            });
        });
    })



//	图片上传
//	数据库中的数据格式为
// [{"color":"红色","url":"http://192.168.25.133/group1/M00/00/01/wKgZhVmHINKADo__AAjlKdWCzvg874.jpg"},
// {"color":"黑色","url":"http://192.168.25.133/group1/M00/00/01/wKgZhVmHINyAQAXHAAgawLS1G5Y136.jpg"}]

	//初始化图片上传的对象
	$scope.imageEntity={};
	$scope.entity.goodsDesc.itemImages=[];
	$scope.fileUpload=function () {
        fileUpLoadService.upload().success(function (response) {
			if(response.success){
                $scope.imageEntity.url=response.message;
                $scope.entity.goodsDesc.itemImages.push($scope.imageEntity);
			}else{
				alert(response.message);
			}
        })
    }
    //删除图片列表
    $scope.deleteImages=function (index,url) {
		$scope.entity.goodsDesc.itemImages.splice(index,1);
    }

//    选择的属性
//	数据结构是[{,[]},...] specificationItems optionName options

	//点击的规格选项数组
	$scope.entity.goodsDesc.specificationItems=[];
    $scope.selectOptions=function (event, specName, optionName) {
		//判断数组中是否存在当前规格
		var specItems = $scope.getObjectByValue($scope.entity.goodsDesc.specificationItems,"attributeName",specName);
		if(specItems==null){
            $scope.entity.goodsDesc.specificationItems.push({"attributeName":specName,"attributeValue":[optionName]});
		}else{
			if(event.target.checked){
				specItems.attributeValue.push(optionName);
			}else{
                var index=specItems.attributeValue.indexOf(optionName);
                specItems.attributeValue.splice(index,1);
                if(specItems.attributeValue.length==0){
                    var index2=$scope.entity.goodsDesc.specificationItems.indexOf(specItems);
                    $scope.entity.goodsDesc.specificationItems.splice(index2,1);
				}
			}
		}
    }

    //点击的规格选项数组
    //构建item列表的方法
    $scope.createItemList=function () {
        // item中当前对象的规格数据spec: {"机身内存":"16G","网络":"联通3G"}
        //声明sku中的对象
        $scope.entity.itemList=[{spec:{},price:0.00,num:999,status:"1",isDefault:"0"}];
        //勾选的规格列表数据
        //[{"attributeName":"网络","attributeValue":["移动3G"]},{"attributeName":"机身内存","attributeValue":["16G"]}]
        var checkSpecList=  $scope.entity.goodsDesc.specificationItems;

        //全部取消勾选的规格结果集
        if(checkSpecList.length==0){
            $scope.entity.itemList=[];
        }
        //遍历$scope.entity.goodsDesc.specificationItems
        for(var i=0;i<checkSpecList.length;i++){
            //抽取方法，为itemList中对象的spec对象属性动态赋值
            $scope.entity.itemList = addColumn($scope.entity.itemList,checkSpecList[i].attributeName,checkSpecList[i].attributeValue);
        }

    }
    //为itemList中对象的spec对象属性动态赋值
    addColumn=function (list,specName,specOptions) {
        //新的item列表
        var newList=[];
        //遍历itemList
        for(var i=0;i<list.length;i++){
            //获取list中的item数据
            //{spec:{},price:0.00,num:999,status:"1",isDefault:"0"}
            var oldItem = list[i];
            //遍历点击选中的规格选项数组
            for(var j=0;j<specOptions.length;j++){
                //基于深克隆机制实现创建新对象
                var newItem = JSON.parse(JSON.stringify(oldItem));
                newItem.spec[specName]=specOptions[j];
                newList.push(newItem);
            }
        }
        return newList;
    }

    //在规格多选框上进行ng-checked的判断
    $scope.checkAttributeValue=function(specName,optionName){
        var items= $scope.entity.goodsDesc.specificationItems;
        var object= $scope.getObjectByValue(items,'attributeName',specName);
        if(object !=null){
            if(object.attributeValue.indexOf(optionName)>=0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
});	
