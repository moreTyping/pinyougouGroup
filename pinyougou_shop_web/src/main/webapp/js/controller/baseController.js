app.controller("baseController",function ($scope) {

    //定义分页控件对象
    $scope.paginationConf = {
        currentPage:1,  				//当前页
        totalItems:10,					//总记录数
        itemsPerPage:10,				//每页记录数
        perPageOptions:[10,20,30,40,50], //分页选项，下拉选择一页多少条记录
        onChange:function(){			//页面变更后触发的方法
            $scope.reloadList();		//启动就会调用分页组件
        }
    };

    $scope.reloadList=function () {
        $scope.search($scope.paginationConf.currentPage,$scope.paginationConf.itemsPerPage);
    }

    //定义封装批量删除的id数组
    $scope.selectIds=[];

    //更新复选框选中状态
    $scope.updateSelection=function ($event,id) {
        //判断复选框勾选还是取消勾选
        //$event事件对象  获取事件源对象 $event.target 就是复选框对象
        if($event.target.checked){
            //勾选操作
            //js 往数组添加值 push()
            $scope.selectIds.push(id);
        }else {
            //取消勾选，从数组中移除当前元素
            var index=$scope.selectIds.indexOf(id);
            //参数一：移除位置的元素的索引值 参数二：从该位置移除几个元素
            $scope.selectIds.splice(index,1);
        }
    }
    
    //解析json数组格式字符串，获取json数组中对象的属性值，做字符串拼接
    $scope.getValueByKey=function (jsonString,key) {
          //解析json数组格式字符串  例如：[{"id":27,"text":"网络"},{"id":32,"text":"机身内存"}]
       var jsonArr = JSON.parse(jsonString);
        var value="";
       for(var i=0;i<jsonArr.length;i++){
           //基于json对象的属性名获取属性值有两种方式
           //1、如果属性名是确定值， 获取方式：对象.属性名  对象[属性名]
            //2、如果属性名是变量， 获取方式：对象[属性名]
            if(i>0){
                value+=","+jsonArr[i][key];
            }else {
                value+=jsonArr[i][key];
            }

       }
       return value;
    }


//    通过值找对象
    $scope.getObjectByValue=function (object, key,value) {
        for(var i=0;i<object.length;i++){
            if(object[i][key]==value){
                return object[i];
            }
        }
        return null;
    }
    
    
});