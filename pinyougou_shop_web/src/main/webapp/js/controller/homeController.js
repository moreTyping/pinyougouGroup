//定义控制器，为name模型数据赋值
//参数一：控制器名称 参数二：控制器要处理的事情
//$scope可以理解为全局的作用域对象  作用：相当于js代码与html代码数据交互的桥梁
//$http内置服务，作用：发起http请求 注意：发起的全部都是异步请求（ajax）
app.controller("homeController",function ($scope,$controller,loginService,homeService) {

    //完成控制器继承代码
    //参数一：父控制器名称 参数二：共享$scope固定写法
    $controller("baseController",{$scope:$scope});

    $scope.Order_OrderItemEntity=[];

    //获取登录名
    $scope.getLoginName=function () {
        loginService.getLoginName().success(function (response) {
            //response={loginName:"admin"}
            $scope.loginName=response.loginName;
            findCountByYear();
        })

    }

    //获取商家一年销售额
    findCountByYear=function () {
        homeService.findCountByYear($scope.loginName).success(function (response) {
            $scope.Order_OrderItemEntity=response;
            showEchars();
        })

        showEchars=function () {
            var data1=[];
            var data2=[];
            //组装数据
            for(var i=0;i< $scope.Order_OrderItemEntity.length;i++){
                data1.push($scope.Order_OrderItemEntity[i].name)
                data2.push($scope.Order_OrderItemEntity[i].value)
            }



            //初始化echarts实例
            var myChart = echarts.init(document.getElementById('main3'));

            var option = {
                title:{
                    text:'年度销售量',
                    subtext: '/件',
                    x:'center'
                },
                tooltip:{},
                legend:{
                    data:['用户来源']
                },
                xAxis:{
                    data:data1
                },
                yAxis:{

                },
                series:[{
                    name:'销售量',
                    type:'line',
                    data:data2
                }]
            };


            //使用制定的配置项和数据显示图表
            myChart.setOption(option);

        }
    }



});
