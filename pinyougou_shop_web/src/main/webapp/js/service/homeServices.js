//服务层
app.service('homeService',function($http){

    this.findAll3Category=function () {
        return $http.get("../home/findAll3Category.do");
    }

    this.findSaleMoneyInMonth=function () {
        return $http.get("../home/findSaleMoneyInMonth.do");
    }
    this.findCountByYear=function (sellerid) {
        return $http.get("../home/findCountByYear.do?sellerid="+sellerid);
    }
});
