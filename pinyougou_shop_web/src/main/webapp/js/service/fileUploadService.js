app.service("fileUpLoadService",function ($http) {
    this.upload=function (){

        var formData = new FormData();

        //获得表单中的要上传的数据 因为没有使用多文件上传 所以直接使用files[0]即可获得
        //参数1是文件上传 后端接收方法参数名称
        //参数2是文件上传 file是前端在input file标签中 id的值

        formData.append("file",file.files[0]);

        return $http({
            url:"../upload/imageUpload.do",
            method:"post",
            data:formData,
            //相当于设置 form表单中的属性 enctype=multipart/form-data  默认是text/plain
            headers:{'Content-Type':undefined},
            //对整个表单进行二进制序列化
            transformRequest:angular.identity
        });
    }

});