app.service("specificationService", function ($http) {

    this.search=function (searchEntity,pageNum,pageSize) {
        return $http.post("../specification/search.do?pageNum="+pageNum+"&pageSize="+pageSize,searchEntity);
    }

    this.save=function (entity) {
        /*if(entity.specification.id!=null){
            //    修改
            return $http.post("../specification/update.do",entity);
        }else{
            //添加
            return $http.post("../specification/save.do",entity);
        }*/
        return $http.post("../specification/save.do",entity);
    }

    this.deleteIds=function (ids) {
        return $http.get("../specification/delete.do?ids="+ids);
    }

    this.findOne=function (id) {
        return $http.get("../specification/findOne.do?specId="+id);
    }

    this.update=function (entity) {
        /*if(entity.specification.id!=null){
            //    修改
            return $http.post("../specification/update.do",entity);
        }else{
            //添加
            return $http.post("../specification/save.do",entity);
        }*/
        return $http.post("../specification/update.do",entity);
    }


})