//服务层
app.service('uploadService',function($http){
	    	
	//文件上传功能
	this.uploadFile=function(){
		//基于angularjs结合html5（表单数据对象）实现文件上传
		var formData = new FormData();
		//基于表单数据对象，追加获取页面选择的上传文件
		//参数一：文件上传，后端接收方法参数名称
		//参数二：文件上传，后端接收方法参数名称   <input type="file" id="file" />
		//  file.files[0] 写法的含义：第一个file，指的是：<input type="file" id="file" />中的id值  ，files[0]是因为获取的是文件数组，需要获取第一个文件
        formData.append("file",file.files[0]);

        return $http({
			url:"../upload/uploadFile.do",
			method:"post",
			data:formData,
            headers : {'Content-Type' : undefined}, //上传文件必须是这个类型，默认text/plain ，相当于指定form的enctype="multipart/form-data"属性
            transformRequest : angular.identity  //对整个表单进行二进制序列化
		});

	}
});
