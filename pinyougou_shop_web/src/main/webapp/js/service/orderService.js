//服务层
app.service('orderService',function($http){
	    	
		//查询订单所有数据
	this.findAll=function () {
		return $http.get("../order/findAll.do");
    };
	//查询spu商品数据列表
	this.findAllItem=function () {
		return $http.get("../order/findAllItem.do");
    };
});
