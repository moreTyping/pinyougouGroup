//定义控制器，为name模型数据赋值
//参数一：控制器名称 参数二：控制器要处理的事情
//$scope可以理解为全局的作用域对象  作用：相当于js代码与html代码数据交互的桥梁
//$http内置服务，作用：发起http请求 注意：发起的全部都是异步请求（ajax）
app.controller("indexController",function ($scope,$controller,contentService) {

    //完成控制器继承代码
    //参数一：父控制器名称 参数二：共享$scope固定写法
    $controller("baseController",{$scope:$scope});

    //根据广告分类查询广告列表数据
    $scope.findByCategoryId=function (categoryId) {
        contentService.findByCategoryId(categoryId).success(function (response) {
            $scope.contentList=response;
        })
    }
    
    //门户网站对接搜索模块
    $scope.search=function () {
        //angularjs进行页面参数传递时，涉及路由传参，需要在请求地址问号前加“#”号
        location.href="http://search.pinyougou.com/search.html#?keywords="+$scope.keywords;
    }


});
