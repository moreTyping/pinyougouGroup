package com.pinyougou.search.service;

import java.util.Map;

public interface SearchService {

    /**
     * 商品搜索方法
     * searchMap:封装搜索条件的map集合
     * {keywords:"",brand:"",price:"" ,sortField:"",sort:"".....}
     */
    public Map<String,Object> search(Map searchMap);
}
