package com.pinyougou.search.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.*;
import org.springframework.data.solr.core.query.result.HighlightEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
@Transactional
public class SearchServiceImpl implements SearchService {

    @Autowired
    private SolrTemplate solrTemplate;

    @Override
    public Map<String, Object> search(Map searchMap) {
        //高亮查询对象
        HighlightQuery query = new SimpleHighlightQuery();
        //1、关键字搜索
        String keywords = (String) searchMap.get("keywords");
        //封装关键字搜索的条件对象
        Criteria criteria = null;
        if(keywords!=null && !"".equals(keywords)){
            //输入了关键字
            criteria=new Criteria("item_keywords").is(keywords);
        }else {
            //没有输关键字，查询所有 *:*
            criteria=new Criteria().expression("*:*");
        }

        //将关键字条件赋予查询对象
        query.addCriteria(criteria);

        //2、分类过滤条件查询
        String category = (String) searchMap.get("category");
        if(category!=null && !"".equals(category)){
            //组装分类过滤条件查询操作
            Criteria categoryCriteria = new Criteria("item_category").is(category);
            FilterQuery filterQuery = new SimpleFilterQuery(categoryCriteria);
            //将过滤条件查询对象赋予主查询对象query
            query.addFilterQuery(filterQuery);
        }

        //3、品牌过滤条件查询
        String brand = (String) searchMap.get("brand");
        if(brand!=null && !"".equals(brand)){
            //组装品牌过滤条件查询操作
            Criteria brandCriteria = new Criteria("item_brand").is(brand);
            FilterQuery filterQuery = new SimpleFilterQuery(brandCriteria);
            //将过滤条件查询对象赋予主查询对象query
            query.addFilterQuery(filterQuery);
        }

        //4、规格过滤条件查询
        Map<String,String> specMap = (Map<String, String>) searchMap.get("spec");
        if(specMap!=null){
            //遍历map，获取规格名称和规格选择的值
            for(String key : specMap.keySet()){
                //组装规格过滤条件查询操作
                Criteria specCriteria = new Criteria("item_spec_"+key).is(specMap.get(key));
                FilterQuery filterQuery = new SimpleFilterQuery(specCriteria);
                //将过滤条件查询对象赋予主查询对象query
                query.addFilterQuery(filterQuery);
            }
        }

        //5、价格区间过滤条件查询
        String price = (String) searchMap.get("price");
        if(price!=null && !"".equals(price)){
            //组装价格区间过滤条件查询操作  0-1000  1000-2000  2000-3000 3000-*   关键：判断临界值 0  *
            String[] prices = price.split("-");

            if(!"0".equals(prices[0])){
                //组装价格区间条件查询操作   大于等于起始值
                Criteria priceCriteria = new Criteria("item_price").greaterThanEqual(prices[0]);
                FilterQuery filterQuery = new SimpleFilterQuery(priceCriteria);
                //将过滤条件查询对象赋予主查询对象query
                query.addFilterQuery(filterQuery);
            }

            if(!"*".equals(prices[1])){
                //组装价格区间条件查询操作   小于等于起始值
                Criteria priceCriteria = new Criteria("item_price").lessThanEqual(prices[1]);
                FilterQuery filterQuery = new SimpleFilterQuery(priceCriteria);
                //将过滤条件查询对象赋予主查询对象query
                query.addFilterQuery(filterQuery);
            }

        }

        //6、排序条件查询
        String sortField = (String) searchMap.get("sortField");
        String sort = (String) searchMap.get("sort");
        if(sortField!=null && !"".equals(sortField)){
            if(sort.equals("ASC")){//升序
                //设置排序条件
                query.addSort(new Sort(Sort.Direction.ASC,"item_"+sortField));
            }else {
                //设置排序条件
                query.addSort(new Sort(Sort.Direction.DESC,"item_"+sortField));
            }
        }

        //7、分页条件查询
        Integer pageNo= (Integer) searchMap.get("pageNo");
        Integer pageSize= (Integer) searchMap.get("pageSize");
        query.setOffset((pageNo-1)*pageSize);//分页起始值  (pageNo-1)*pageSize 0-59  60-119
        query.setRows(pageSize);//每页显示记录数据

        //设置高亮
        //高亮对象
        HighlightOptions highlightOptions = new HighlightOptions();
        //设置高亮字段
        highlightOptions.addField("item_title");
        //设置高亮前缀和后缀
        highlightOptions.setSimplePrefix("<font color='red'>");
        highlightOptions.setSimplePostfix("</font>");
        query.setHighlightOptions(highlightOptions);

        //条件分页高亮查询
        HighlightPage<TbItem> page = solrTemplate.queryForHighlightPage(query, TbItem.class);

        //获取当前页商品列表数据
        List<TbItem> content = page.getContent();

        //替换高亮内容
        for (TbItem item : content) {
            //获取高亮内容
            List<HighlightEntry.Highlight> highlights = page.getHighlights(item);
            if(highlights.size()>0){
                //有高亮内容
                HighlightEntry.Highlight highlight = highlights.get(0);
                //获取高亮内容
                List<String> snipplets = highlight.getSnipplets();
                if(snipplets.size()>0){
                    item.setTitle(snipplets.get(0));
                }
            }
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("rows",content);
        resultMap.put("totalPages",page.getTotalPages());
        resultMap.put("pageNo",pageNo);

        return resultMap;
    }
}
